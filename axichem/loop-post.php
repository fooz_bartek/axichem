<div class="loop"> <!-- loop-front -->
	<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post(); ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class('single row'); ?>>
								<div class="content_wrapper">
									<?php
									the_content();
									?>
								</div>
						</article>
		<?php	} // end while
		}
		else {
		?>
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h2>
					<?php echo _e( 'Nothing to Show Right Now', 'theme'); ?>
						</h2>
					</div>
				</div>
			</div>
		<?php
		} // end if
	wp_reset_query();
	//wp_reset_postdata()
	?>
</div> <!-- /.loop -->
