
<div class="loop">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title">
				<?php
				if(function_exists('bcn_display')){
					?>
					<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
						<?php
							bcn_display();
						?>
					</div>
					<?php
				}
				?>
				<h1><?php
					$titlepage = get_simple_title();
					if($titlepage == ""){
						if (is_search()){
							$titlepage = __('Search resoults', 'theme');
						}
					}
					echo $titlepage;
				?></h1>
			</div>
		</div>
	</div>
<?php
	$counter = 1;
	$imgatr=array(
		'class' => 'img-responsive'
	);
	global $wp_query;
	$count = $wp_query->post_count;

	if ( have_posts() ) {
		?>
		<div class="row">
		<?php
    while ( have_posts() ) {
			the_post();
			if($counter == 1){
				echo '<article class="first-loop col-xs-12"> ';
				$excerpt = 500;
			}
				if($counter == 2){
					echo '<div class="loop-wrapper row-eq-height">';
				}
				if($counter > 1){
					echo '<article class="element-loop col-xs-12 col-sm-6 col-md-4 col-lg-3" > ';
					$excerpt = 200;
				}
						if($counter == 1){
							echo '<h3>';
						} else {
							echo '<h4>';
						}
							echo '<a href="' . get_permalink() . '">' . get_the_title() . '</a>';
						if($counter == 0){
							echo '</h3>';
						} else {
							echo '</h4>';
						}

						if($counter == 1){
							the_post_thumbnail('post-thumbnails', $imgatr);
						}

						?>
							<div class="post-date">
								<span class="post-date"><?php the_time('d.m.Y'); ?></span>
							</div>
							<?php
							/*
							<div class="post-category">
								<?php the_category(', '); ?>
							</div>
							*/
						echo the_excerpt_max_charlength($excerpt);

						echo '<div class="read-more-wrapper clearfix text-right"><a href="'.get_permalink().'" title=".get_the_title()." class="read-more">'.__('Read more...', 'theme').'</a></div>';

				if($counter > 1){
					echo '</article> '; // end col
				}
			if($counter == 1){
				echo '</article> '; // end col-sm-12 col-md-8 col-lg-8
			}
			if($counter == $count){
				echo '</div>'; //end div loop-wrapper
			}
			$counter++;

    } // end while
		?>
		</div>

      <div class="post-navigation">
        <div class="alignleft">
          <?php  previous_posts_link( '<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>'.__(' Newer posts', 'theme') ); ?>
        </div>

        <div class="alignright">
          <?php next_posts_link( __('Older posts ', 'theme').'<i class="fa fa-chevron-circle-right" aria-hidden="true"></i>' ); ?>
        </div>
      </div>
    <?php
	}
	else {
	?>
		<div class="row">
			<div class="col-xs-12">
				<h2>
			<?php echo _e( 'Nothing to Show Right Now', 'theme'); ?>
				</h2>
			</div>
		</div>
	<?php
	} // end if
	?>

<?php
wp_reset_query();
wp_reset_postdata()
?>
</div> <!-- /.row -->

<!-- Next and previous categories -->

</div>
