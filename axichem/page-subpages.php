<?php
/*
    Template Name: Page + List Subpages
*/
get_header();
?>

<?php
$actId = get_the_ID();
$page = get_post(get_the_ID());
$parentId = get_the_ID();
$parentTitle = $childrenTitle = $page->post_title;
$childrenContent = $page->post_content;

if($page->post_parent != 0){
    $page = get_post($page->post_parent);
    $parentId = $page->ID;
    $parentTitle = $page->post_title;
}
?>

<div class="shortcode_pagesdisplay">
    <div class="top">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="header text38"><?php echo $parentTitle; ?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="pages">
        <div class="container">
            <div class="row">
                <div class="col col-12 col-lg-3">
                    <div class="links">
                        <?php
                        $queryPages = new WP_Query(
                            array(
                                'post_type' => 'page',
                                'post_parent' => $parentId,
                                'posts_per_page' => -1,
                                'order' => "DESC",
                                'orderby' => 'date',
                                'ignore_sticky_posts' => 1,
                                'post_status' => 'publish'
                            )
                        );
                        $x = 1;
                        while ($queryPages->have_posts()) : $queryPages->the_post();
                            $class = '';
                            if($actId == get_the_ID())
                                $class = 'active';
                            echo '<a class="'. $class .'" href="'. get_permalink() .'">'. get_the_title() .'</a>';
                        endwhile;
                        ?>
                    </div>
                </div>
                <div class="col col-12 col-lg-7 offset-lg-1">
                    <div class="title_page text38"><?php echo $childrenTitle; ?></div>
                    <div class="content text15">
                        <?php
                        $content = preg_replace('/<h1>/i', '<h1 class="text38">', $childrenContent);
                        $content = preg_replace('/<h2>/i', '<h2 class="text38">', $content);
                        $content = preg_replace('/<h3>/i', '<h3 class="text38">', $content);
                        $content = preg_replace('/<h4>/i', '<h4 class="text32">', $content);
                        $content = preg_replace('/<h5>/i', '<h5 class="text32">', $content);
                        $content = preg_replace('/<h6>/i', '<h6 class="text32">', $content);
                        echo do_shortcode($content);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>
