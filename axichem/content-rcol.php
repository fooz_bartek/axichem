<!-- content r-col -->
<section class="container" id="content">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-8">
			<?php
					get_template_part('loop');
			?>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4">
			<?php
				if ( is_active_sidebar( 'right-sidebar' ) ) {
					dynamic_sidebar( 'right-sidebar' );
				}
			?>
		</div>
	</div>
</section>
