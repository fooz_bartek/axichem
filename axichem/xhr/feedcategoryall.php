<?php
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
    require('../../../../wp-load.php');
else
    require('wp-load.php');
?>

<?php
// Header
if(strlen($_GET['header']) > 0)
    $header = $_GET['header'];

// Category
if(strlen($_GET['category']) > 0)
    $category = $_GET['category'];

$tax_query = array();
$tax_query[] = array(
    'taxonomy' => 'type',
    'field' => 'slug',
    'terms' => $category
);

// Year
if(strlen($_GET['year']) > 0)
    $year = $_GET['year'];

$date_query = array();
$date_query = array(
    'year' => $year
);
?>

<div class="shortcode_header text38"><?php echo $header; ?></div>

<div class="item show">
<?php
$queryFeed = new WP_Query(
    array(
        'post_type' => 'news',
        'posts_per_page' => -1,
        'date_query' => $date_query,
        'tax_query' => $tax_query,
        'order' => "DESC",
        'orderby' => 'date',
        'ignore_sticky_posts' => 1,
        'post_status' => 'publish'
    )
);
?>

<?php if($queryFeed->post_count == 0){ ?>
    No results
<?php } ?>

<?php
$x = 1;
while ($queryFeed->have_posts()) : $queryFeed->the_post();
?>

<div class="post">
    <div class="col-first">
        <div class="date text15"><?php echo __(get_the_date('F d, Y')); ?></div>
        <a class="title text22" href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>
        <div class="excerpt text15">
            <?php echo limit_words(get_the_excerpt(), 10); ?>
            <a class="readmore" href="<?php echo get_permalink(); ?>">
                <?php
                $read_more = 'Read the article here';
                if(strlen(get_field('read_more', get_the_ID())) > 0)
                    $read_more = get_field('read_more', get_the_ID());
                echo __($read_more, 'axichem');
                ?>
            </a>
        </div>
    </div>
    <div class="col-second">
        <?php if(get_post_meta(get_the_ID(), 'FilesUrl', true) ){ ?>
            <a class="download" href="<?php echo get_post_meta(get_the_ID(), 'FilesUrl', true ); ?>" target="_blank">
                <svg x="0px" y="0px" width="24px" height="27px" viewBox="0 0 24 27" style="enable-background:new 0 0 24 27;" xml:space="preserve">
                    <line stroke-width="2" fill="none" x1="12" y1="0" x2="12" y2="18"></line>
                    <polyline stroke-width="1" fill="none" points="1,18 1,26 23,26 23,18"></polyline>
                    <polyline stroke-width="1" fill="none" points="6,11 12.5,17.5 18.7,11.3"></polyline>
                </svg>
            </a>
        <?php } ?>
        <a class="arrow" href="<?php echo get_permalink(); ?>">
            <svg x="0px" y="0px" width="25.623px" height="25.623px" viewBox="0 0 25.623 25.623" enable-background="new 0 0 25.623 25.623" xml:space="preserve">
                <circle fill="none" stroke-miterlimit="10" cx="12.811" cy="12.812" r="12.311"/>
                <line fill="none" stroke-miterlimit="10" x1="19.709" y1="13.051" x2="4.709" y2="13.051"/>
                <polyline fill="none" stroke-miterlimit="10" points="12.948,5.876 19.454,12.635 12.949,19.349 "/>
            </svg>
        </a>
    </div>
</div>

<?php
    $x++;
endwhile;
wp_reset_query();
?>
</div>
