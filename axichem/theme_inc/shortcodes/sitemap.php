<?php
function shortcode_Sitemap( $atts ) {
    ob_start();
?>

<div class="shortcode shortcode_sitemap">
    <div class="container">
        <div class="row">
            <div class="col col-12 col-md-3">
                <div class="row">
                    <div class="col col-12 col-md-4 link_group">
                        <a class="link_main text24" href="<?php echo get_permalink(icl_object_id(6)); ?>"><?php echo get_the_title(icl_object_id(6)); ?></a>
                    </div>
                </div>
            </div>
            <div class="col col-12 col-md-9">
                <div class="row">
                <?php
                $queryPages = new WP_Query(
                    array(
                        'post_type' => 'page',
                        'post_parent' => 0,
                        'posts_per_page' => -1,
                        'order' => "DESC",
                        'orderby' => 'date',
                        'ignore_sticky_posts' => 1,
                        'post_status' => 'publish'
                    )
                );
                $x = 1;
                while ($queryPages->have_posts()) : $queryPages->the_post();
                    global $post;
                    if ($post->post_name != 'home'){
                ?>

                <div class="col col-12 col-sm-6 col-md-4 link_group">
                    <a class="link_main text22" href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>

                    <?php
                    $queryPagesSub = new WP_Query(
                        array(
                            'post_type' => 'page',
                            'post_parent' => $post->ID,
                            'posts_per_page' => -1,
                            'order' => "DESC",
                            'orderby' => 'menu_order',
                            'ignore_sticky_posts' => 1,
                            'post_status' => 'publish'
                        )
                    );
                    $x = 1;
                    while ($queryPagesSub->have_posts()) : $queryPagesSub->the_post();
                        global $post;
                    ?>

                    <a class="link_sub text15" href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>

                    <?php endwhile; ?>
                </div>

                <?php
                    }
                endwhile;
                ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$display = ob_get_contents();
ob_end_clean();
return $display;
}
add_shortcode('sitemap', 'shortcode_Sitemap' );
