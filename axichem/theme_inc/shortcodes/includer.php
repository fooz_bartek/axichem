<?php
// Shortcode

require_once(THEME_SC_DIRECTORY.'feed_selected.php');
require_once(THEME_SC_DIRECTORY.'feed_category.php');
require_once(THEME_SC_DIRECTORY.'feed_category_all.php');

require_once(THEME_SC_DIRECTORY.'files.php');

require_once(THEME_SC_DIRECTORY.'events.php');
require_once(THEME_SC_DIRECTORY.'stock_exchange.php');
require_once(THEME_SC_DIRECTORY.'scientific_reports.php');
require_once(THEME_SC_DIRECTORY.'people.php');
require_once(THEME_SC_DIRECTORY.'meetings.php');
require_once(THEME_SC_DIRECTORY.'financial-calendar.php');

require_once(THEME_SC_DIRECTORY.'contact.php');

require_once(THEME_SC_DIRECTORY.'pages_display.php');
require_once(THEME_SC_DIRECTORY.'pressroom_display.php');
require_once(THEME_SC_DIRECTORY.'sitemap.php');
