<?php
function shortcode_ScientificReports( $atts ) {
    ob_start();
?>

<div class="shortcode shortcode_scroll shortcode_scientificreports">
    <div class="col-full"><div class="shortcode_header text38"><?php echo __('Latest Scientific Reports', 'axichem'); ?></div></div>

    <div class="posts items_container" data-p="0">
        <div class="items">
            <div class="item">
            <?php
            $queryOfferAll = new WP_Query(
                array(
                    'post_type' => 'reports',
                    'posts_per_page' => 20,
                    'order' => "DESC",
                    'orderby' => 'date',
                    'ignore_sticky_posts' => 1,
                    'post_status' => 'publish'
                )
            );
            $x = 1;
            while ($queryOfferAll->have_posts()) : $queryOfferAll->the_post();
            ?>

            <div class="post">
                <div class="col-first">
                    <div class="date text15"><?php echo __(get_the_date('F d, Y')); ?></div>
                    <div class="title text22" href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></div>
                    <div class="excerpt text15">
                        <?php echo limit_words(get_the_excerpt(), 15); ?>
                    </div>
                </div>
                <div class="col-second">
                    <div class="data text13"><?php echo nl2br(get_field('descriptions', get_the_ID())); ?></div>
                    <div class="data text13"><?php echo nl2br(get_field('authors', get_the_ID())); ?></div>
										<?php if(sizeof(get_field('file', get_the_ID())) > 1){ ?>
                        <a class="download" href="<?php echo get_field('file', get_the_ID())['url']; ?>" target="_blank">
                            <svg x="0px" y="0px" width="24px" height="27px" viewBox="0 0 24 27" style="enable-background:new 0 0 24 27;" xml:space="preserve">
                            	<line stroke-width="2" fill="none" x1="12" y1="0" x2="12" y2="18"/>
                            	<polyline stroke-width="1" fill="none" points="1,18 1,26 23,26 23,18" />
                            	<polyline stroke-width="1" fill="none" points="6,11 12.5,17.5 18.7,11.3" />
                            </svg>
                        </a>
                    <?php } ?>
										<?php if(strlen(get_field('file_external', get_the_ID())) > 1){ ?>
										    <a class="download" href="<?php echo get_field('file_external', get_the_ID()); ?>" target="_blank">
                            <svg x="0px" y="0px" width="24px" height="27px" viewBox="0 0 24 27" style="enable-background:new 0 0 24 27;" xml:space="preserve">
                            	<line stroke-width="2" fill="none" x1="12" y1="0" x2="12" y2="18"/>
                            	<polyline stroke-width="1" fill="none" points="1,18 1,26 23,26 23,18" />
                            	<polyline stroke-width="1" fill="none" points="6,11 12.5,17.5 18.7,11.3" />
                            </svg>
                        </a>
                    <?php } ?>
                </div>
            </div>

            <?php
            if($x%5 == 0)
                echo '</div><div class="item">';
            ?>

            <?php
                $x++;
            endwhile;
            wp_reset_query();
            ?>
            </div>
        </div>
    </div>
    <div class="navigation">
        <div class="arrows">
            <a class="next">
                <svg x="0px" y="0px" width="11.965px" height="6.818px" viewBox="0 0 11.965 6.818" enable-background="new 0 0 11.965 6.818" xml:space="preserve">
                <polyline fill="none" stroke-width="2" stroke-miterlimit="10" points="11.604,0.354 6.104,6.104 0.354,0.354 "/>
                </svg>
            </a>
            <a class="prev off">
                <svg x="0px" y="0px" width="11.965px" height="6.818px" viewBox="0 0 11.965 6.818" enable-background="new 0 0 11.965 6.818" xml:space="preserve">
                <polyline fill="none" stroke-width="2" stroke-miterlimit="10" points="0.361,6.465 5.861,0.715 11.611,6.465 "/>
                </svg>
            </a>
        </div>
    </div>
</div>



<?php
$display = ob_get_contents();
ob_end_clean();
return $display;
}
add_shortcode('scientific_reports', 'shortcode_ScientificReports' );
