<?php
function shortcode_CisionFeedCategory( $atts ) {
    ob_start();
?>

<div class="shortcode shortcode_scroll shortcode_feedcategory">
    <div class="row">
        <div class="col-12 col-md-5 col-lg-3">
            <div class="filter active">
                <form>
                    <div class="title text15"><?php echo __($atts['header'], 'axichem'); ?></div>
                    <div class="select_category">
                        <select class="niceselect" name="category">
                            <?php
                            $terms = get_terms('type', array(
                                'hide_empty' => false,
                            ));
                            $categories = explode(',', $atts['categories']);
                            $term_first = $header_first = '';
                            $term_sum = '';
                            foreach ($categories as $key => $value) {
                                foreach ($terms as $keyTerm => $valueTerm) {
                                    if($valueTerm->slug == $value){
                                        echo '<option value="'. $valueTerm->slug .'" data-name="'. $valueTerm->name .'">'. $valueTerm->name .'</option>';
                                        $term_sum .= $valueTerm->slug.',';
                                        if(strlen($term_first) == 0){
                                            $term_first = $valueTerm->slug;
                                            $header_first = $valueTerm->name;
                                        }
                                    }
                                }
                            }
                            ?>
                            <option value="<?php echo $term_sum; ?>" data-name="All"><?php _e('All', 'axichem'); ?></option>
                        </select>
                    </div>
                    <div class="select_year">
                        <select class="niceselect" name="year">
                        <?php
                        for ($i = 0; $i <= date('Y') - 2008; $i++) {
                            echo '<option value="'. (date('Y') - $i) .'">'. (date('Y') - $i) .'</option>';
                        }
                        ?>
                        </select>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-12 col-md-7 col-lg-6 offset-lg-1">
						<div class="shortcode_header text38"><?php echo $header_first; ?></div>
            <div class="posts items_container" data-p="0">
                <div class="items">
                    <?php
                    $header = $header_first;
                    $category = $term_first;
                    $year = date('Y');
                    include getcwd() . '/wp-content/themes/axichem/xhr/feedcategory.php';
                    ?>
                </div>
            </div>
            <div class="navigation">
                <div class="arrows">
                    <a class="next">
                        <svg x="0px" y="0px" width="11.965px" height="6.818px" viewBox="0 0 11.965 6.818" enable-background="new 0 0 11.965 6.818" xml:space="preserve">
                        <polyline fill="none" stroke-width="2" stroke-miterlimit="10" points="11.604,0.354 6.104,6.104 0.354,0.354 "/>
                        </svg>
                    </a>
                    <a class="prev off">
                        <svg x="0px" y="0px" width="11.965px" height="6.818px" viewBox="0 0 11.965 6.818" enable-background="new 0 0 11.965 6.818" xml:space="preserve">
                        <polyline fill="none" stroke-width="2" stroke-miterlimit="10" points="0.361,6.465 5.861,0.715 11.611,6.465 "/>
                        </svg>
                    </a>
                </div>
                <a class="readmore text15" href="<?php echo get_permalink(icl_object_id(765)); ?>"><?php _e('Read more', 'axichem'); ?></a>
            </div>
        </div>
    </div>
</div>



<?php
$display = ob_get_contents();
ob_end_clean();
return $display;
}
add_shortcode('feed_category', 'shortcode_CisionFeedCategory' );
