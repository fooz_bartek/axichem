<?php
function shortcode_FinancialCalendar( $atts ) {
    ob_start();
?>

<div class="shortcode shortcode_financialcalendar">
    <div class="shortcode_header text38"><?php echo __('Financial Calendar', 'axichem'); ?></div>
    <div class="events">
        <?php
        $perpage = 2;
        if(is_array($atts) && isset($atts['perpage']))
            $perpage = $atts['perpage'];
        $queryOfferAll = new WP_Query(
            array(
                'post_type' => 'financial-calendar',
                'posts_per_page' => $perpage,
                'order' => "DESC",
                'orderby' => 'date',
                'ignore_sticky_posts' => 1,
                'post_status' => 'publish'
            )
        );
        $x = 1;
        while ($queryOfferAll->have_posts()) : $queryOfferAll->the_post();
        ?>

        <div class="event">
            <div class="date text15"><?php echo __(get_the_date('F d, Y')); ?></div>
            <div class="title text22"><?php echo get_the_title(); ?></div>
        </div>

        <?php
            $x++;
        endwhile;
        wp_reset_query();
        ?>

        </div>
    </div>

    <?php
    $display = ob_get_contents();
    ob_end_clean();
    return $display;
    ?>
<?php
}
add_shortcode('financial_calendar', 'shortcode_FinancialCalendar' );
