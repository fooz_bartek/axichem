<?php
function shortcode_CisionFeedCategoryAll( $atts ) {
    ob_start();
?>

<div class="shortcode shortcode_feedcategoryall">
    <div class="row">
        <div class="col-12 col-lg-3">
            <?php
            $header = '';

            $terms = get_terms('type', array(
                'hide_empty' => false,
            ));
            $categories = array('prm_regulatory','prm', 'inb','nbr');
            $term_first = '';
            foreach ($categories as $key => $value) {
                foreach ($terms as $keyTerm => $valueTerm) {
                    if($valueTerm->slug == $value){
                        $active = '';
                        if($_GET['cat'] == $valueTerm->slug){
                            $active = 'active';
                            $header = $valueTerm->name;
                        }
                    }
                }
            }
            if(!$_GET['cat'])
                $active = 'active';
            ?>
            <div class="filter <?php echo $active; ?>">
                <form>
                    <div class="title text15"><?php _e('All Press Releases', 'axichem'); ?></div>
                    <div class="select_category">
                        <select class="niceselect" name="category">
                            <?php
                            $term_first = '';
                            foreach ($categories as $key => $value) {
                                foreach ($terms as $keyTerm => $valueTerm) {
                                    if($valueTerm->slug == $value){
                                        $selected = '';
                                        if($_GET['cat'] == $valueTerm->slug)
                                            $selected = 'selected';
                                        echo '<option value="'. $valueTerm->slug .'" '. $selected .' data-name="'. $valueTerm->name .'">'. $valueTerm->name .'</option>';

                                        if($term_first == '')
                                            $term_first = $valueTerm->slug;
                                            if(strlen($header) == 0){
                                                $header = $valueTerm->name;
                                            }
                                    }
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="select_year">
                        <select class="niceselect" name="year">
                        <?php
                        for ($i = 0; $i <= date('Y') - 2008; $i++) {
                            echo '<option value="'. (date('Y') - $i) .'">'. (date('Y') - $i) .'</option>';
                        }
                        ?>
                        </select>
                    </div>
                    <div class="clear"></div>
                </form>
            </div>


            <?php
            $terms = get_terms('type', array(
                'hide_empty' => false,
            ));
            $categories = array('kmk','rdv','rpt');
            foreach ($categories as $key => $value) {
                foreach ($terms as $keyTerm => $valueTerm) {
                    if($valueTerm->slug == $value){
                        $active = '';
                        if($_GET['cat'] == $valueTerm->slug){
                            $active = 'active';
                            if(strlen($header) == 0)
                                $header = $valueTerm->name;
                        }
                    }
                }
            }
            ?>
            <div class="filter <?php echo $active; ?>">
                <form>
                    <div class="title text15"><?php _e('Financial Reports', 'axichem'); ?></div>
                    <div class="select_category">
                        <select class="niceselect" name="category">
                            <?php
                            foreach ($categories as $key => $value) {
                                foreach ($terms as $keyTerm => $valueTerm) {
                                    if($valueTerm->slug == $value){
                                        $selected = '';
                                        if($_GET['cat'] == $valueTerm->slug)
                                            $selected = 'selected';
                                        echo '<option value="'. $valueTerm->slug .'" '. $selected .' data-name="'. $valueTerm->name .'">'. $valueTerm->name .'</option>';
                                    }
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="select_year">
                        <select class="niceselect" name="year">
                        <?php
                        for ($i = 0; $i <= date('Y') - 2008; $i++) {
                            echo '<option value="'. (date('Y') - $i) .'">'. (date('Y') - $i) .'</option>';
                        }
                        ?>
                        </select>
                    </div>
                    <div class="clear"></div>
                </form>
            </div>

            <?php
            $active = '';
            if($_GET['cat'] == 'news')
                $active = 'active';
            ?>
            <div class="filter last <?php echo $active; ?>"><a class="title text15 news" data-cat="news" data-name="<?php _e('News', 'axichem'); ?>"><?php _e('News', 'axichem'); ?></a></div>

            <div class="links forPc">
                <?php
                $menuOrg = wp_get_nav_menu_items('Menu PressRoom');
                foreach ($menuOrg as $item) {
                    $url = get_permalink($item->object_id);
                    if ($item->type == 'custom') {
                        $url = $item->url;
                    }
                    $getpost = get_post($item->object_id);
                    echo '<a href="'. $url .'" class="'. $item->classes[0] .'" data-id="'. $item->object_id .'">'. $getpost->post_title. '</a>';
                }
                ?>
            </div>
        </div>
        <div class="col-12 col-lg-7 offset-lg-1">
            <?php
            $page = get_post(get_the_ID());
            $parentId = get_the_ID();

            if($page->post_parent != 0){
                $page = get_post($page->post_parent);
                $parentId = $page->ID;
                $view = 'sub';
            }else{
                $view = 'main';
            }
            ?>
            <div class="posts items_container <?php if($view == 'sub') echo 'hide'; ?>" data-p="0">
                <div class="items">
                    <?php
                    $header = __($header, 'axichem');
                    $category = $term_first;
                    if(strlen($_GET['cat']) > 0)
                        $category = $_GET['cat'];

                    $year = date('Y');
                    include getcwd() . '/wp-content/themes/axichem/xhr/feedcategoryall.php';
                    ?>
                </div>
            </div>

            <div class="links forPhone forTablet">
                <?php
                $menuOrg = wp_get_nav_menu_items('Menu PressRoom');
                foreach ($menuOrg as $item) {
                    $url = get_permalink($item->object_id);
                    if ($item->type == 'custom') {
                        $url = $item->url;
                    }
                    $getpost = get_post($item->object_id);
                    echo '<a href="'. $url .'" class="'. $item->classes[0] .'" data-id="'. $item->object_id .'">'. $getpost->post_title. '</a>';
                }
                ?>
            </div>

            <div class="contents"><?php the_content(); ?></div>
        </div>
    </div>
</div>

<?php
$display = ob_get_contents();
ob_end_clean();
return $display;
}
add_shortcode('feed_category_all', 'shortcode_CisionFeedCategoryAll' );
