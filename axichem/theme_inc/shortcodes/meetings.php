<?php
function shortcode_Meeting( $atts ) {
    ob_start();
?>

<?php
$meetingArray = array();
$queryOfferAll = new WP_Query(
    array(
        'post_type' => 'meetings',
        'posts_per_page' => -1,
        'order' => "DESC",
        'orderby' => 'date',
        'ignore_sticky_posts' => 1,
        'post_status' => 'publish'
    )
);
while ($queryOfferAll->have_posts()) : $queryOfferAll->the_post();
    $terms = get_the_terms( get_the_ID(), 'meetings-category');
    if(!$meetingArray[$terms[0]->name])
        $meetingArray[$terms[0]->name] = array();
    array_push($meetingArray[$terms[0]->name], get_the_ID());
endwhile;
wp_reset_query();

krsort($meetingArray);
?>


<div class="shortcode shortcode_meetings">
    <div class="nav_container">
        <a class="arrow prev">
            <svg x="0px" y="0px" width="8.779px" height="13.273px" viewBox="-4.902 4.351 8.779 13.273" enable-background="new -4.902 4.351 8.779 13.273">
            <polyline fill="none" stroke-width="2" stroke-miterlimit="10" points="2.875,16.6 -2.875,11.1 2.875,5.354"/>
            </svg>
        </a>
        <a class="arrow next">
            <svg x="0px" y="0px" width="8.779px" height="13.273px" viewBox="-3.877 4.33 8.779 13.273" enable-background="new -3.877 4.33 8.779 13.273">
            <polyline fill="none" stroke-width="2" stroke-miterlimit="10" points="-2.875,5.354 2.875,10.854 -2.875,16.6 "/>
            </svg>
        </a>
        <div class="nav">
            <div class="scroll" data-pos="0">
                <?php
                $active = 'active';
                foreach ($meetingArray as $key => $value) { ?>
                    <a class="text13 <?php echo $active; ?>" data-year="<?php echo $key; ?>"><?php echo $key; ?></a>
                <?php
                    $active = '';
                }
                ?>
                <div class="line"></div>
            </div>
        </div>
    </div>
    <div class="meetings_container">
        <?php
        $mime_type = get_allowed_mime_types();
        $mime_type = array_flip($mime_type);
        $active = 'active';
        foreach ($meetingArray as $key => $value) { ?>

            <div class="meet_container <?php echo $active; ?>" data-year="<?php echo $key; ?>">
                <?php foreach ($value as $keyMeet => $valueMeet) { ?>
                    <div class="meet">
                        <div class="title text22"><?php echo get_the_title($valueMeet); ?></div>
                        <div class="info text15">
                            <?php
                            $filesize = filesize( get_attached_file( get_field('file', $valueMeet)['ID'] ) );
                            $filesize = size_format($filesize, 2);
                            echo $mime_type[get_field('file', $valueMeet)['mime_type']].' '.$filesize;
                            ?>
                        </div>
                        <a class="arrow" href="<?php echo get_field('file', $valueMeet)['url']; ?>" target="_blank">
                            <svg x="0px" y="0px" width="25.623px" height="25.623px" viewBox="0 0 25.623 25.623" enable-background="new 0 0 25.623 25.623" xml:space="preserve">
                                <circle fill="none" stroke-miterlimit="10" cx="12.811" cy="12.812" r="12.311"/>
                                <line fill="none" stroke-miterlimit="10" x1="19.709" y1="13.051" x2="4.709" y2="13.051"/>
                                <polyline fill="none" stroke-miterlimit="10" points="12.948,5.876 19.454,12.635 12.949,19.349 "/>
                            </svg>
                        </a>
                    </div>
                <?php } ?>
            </div>
        <?php
            $active = '';
        }
        ?>
    </div>
</div>

<?php
    $display = ob_get_contents();
    ob_end_clean();
    return $display;
}
add_shortcode('meetings', 'shortcode_Meeting' );
