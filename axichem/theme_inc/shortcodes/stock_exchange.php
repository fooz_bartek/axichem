<?php
function shortcode_StockExchange( $atts ) {
    ob_start();
    $shareOptions = get_option('Share');
?>

    <div class="shortcode shortcode_stockexchange">
        <div class="data_main">
            <div class="text1 text22">AXICHEM A</div>
            <div class="text2 text15">(AXIC A)</div>
            <div class="share_price">
                <div class="actual text44">
                    <?php echo $shareOptions['Share_Price']; ?>
                    <div class="currency text15">SEK</div>
                    <div class="trend <?php echo $shareOptions['Share_Trend']; ?>">
                        <svg class="up" x="0px" y="0px" width="14.937px" height="14.937px" viewBox="0 0 14.937 14.937" enable-background="new 0 0 14.937 14.937" xml:space="preserve">
                            <polyline fill="none" stroke-width="3" stroke="#90d62c" stroke-miterlimit="10" points="13.854,13.354 13.854,0.854 0.354,0.854 "/>
                            <line fill="none" stroke-width="3" stroke="#90d62c" stroke-miterlimit="10" x1="0.354" y1="13.854" x2="13.354" y2="0.854"/>
                        </svg>
                        <svg class="down" x="0px" y="0px" width="14.937px" height="14.937px" viewBox="0 0 14.937 14.937" enable-background="new 0 0 14.937 14.937" xml:space="preserve">
                            <polyline fill="none" stroke-width="3" stroke="#bb5355" stroke-miterlimit="10" points="1.354,13.854 13.854,13.854 13.854,0.354 "/>
                            <line fill="none" stroke-width="3" stroke="#bb5355" stroke-miterlimit="10" x1="0.354" y1="0.354" x2="13.354" y2="13.354"/>
                        </svg>
                    </div>
                    <div class="value text15"><?php echo round($shareOptions['Share_Delta'], 2); ?> / <?php echo round($shareOptions['Share_DeltaPercentage'] * 100, 2); ?>%</div>
                </div>
            </div>
        </div>
        <div class="data_param">
            <div class="name text15"><?php echo __('High','axichem')?></div>
            <div class="value text15"><?php echo $shareOptions['Share_HighPrice']; ?></div>
        </div>
        <div class="data_param">
            <div class="name text15"><?php echo __('Low','axichem')?></div>
            <div class="value text15"><?php echo $shareOptions['Share_LowPrice']; ?></div>
        </div>
        <div class="data_param">
            <div class="name text15"><?php echo __('Volume','axichem')?></div>
            <div class="value text15"><?php echo $shareOptions['Share_Quantity']; ?></div>
        </div>
        <div class="data_param">
            <div class="name text15"><?php echo __('Turnover','axichem')?></div>
            <div class="value text15"><?php echo $shareOptions['Share_Turnover']; ?></div>
        </div>
        <div class="data_param">
            <div class="name"></div>
            <div class="value"></div>
        </div>
        <a class="readmore text15" href="http://www.nasdaqomxnordic.com/shares/microsite?Instrument=SSE80256" target="_blank"><?php echo __('Read more','axichem')?></a>
        <div class="date text15">
            <?php
            $date = $shareOptions['Share_QuoteTime'];
            echo __(date('F', strtotime(substr($date, 0, 10)))).' ';
            echo date('d, Y', strtotime(substr($date, 0, 10)));
            echo ' ';
            echo substr($date, 11, 8);
            ?>
        </div>
    </div>

<?php
$display = ob_get_contents();
ob_end_clean();
return $display;
}
add_shortcode('stock_exchange', 'shortcode_StockExchange' );
