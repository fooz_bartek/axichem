<?php
$widget_name = "history";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<a rel="<?php echo $instance['uniq_id']; ?>"></a>
<div id="<?php echo $widget_id; ?>" class="widget widget-history so-wrapper so-panel widget" data-animationcss="anim_opacity" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
        <div class="container">
            <div class="row header">
                <div class="col-12 col-md-4">
                    <div class="text1 text15"><?php echo $instance['text1']; ?></div>
                </div>
                <div class="col-12 col-md-6 offset-md-1">
                    <div class="text2 text24"><?php echo $instance['text2']; ?></div>
                </div>
            </div>

            <div class="row history">
                <div class="timeline start">
                    <div class="col-12">
                        <a class="nav prev">
                            <svg width="11.8px" height="21.7px" viewBox="-126.6 7.5 11.8 21.7" style="enable-background:new -126.6 7.5 11.8 21.7;" xml:space="preserve">
                            	<polyline fill="none" stroke-width="2" points="-115.5,28.8 -126.2,18.6 -115.5,7.9"/>
                            </svg>
                        </a>
                        <a class="nav next">
                            <svg width="11.8px" height="21.7px" viewBox="-126.6 7.5 11.8 21.7" style="enable-background:new -126.6 7.5 11.8 21.7;" xml:space="preserve">
                            	<polyline fill="none" stroke-width="2" points="-126.2,7.9 -115.5,18.1 -126.2,28.8"/>
                            </svg>
                        </a>
                        <div class="timeline_container">
                            <div class="scroll">
                                <?php
                                $active = 'active';
                                foreach ($instance['history'] as $key => $value) {
                                    $last = '';
                                    if($key == sizeof($instance['history'])-1)
                                        $last = 'last';

                                    $delta = '0px';
                                    if($key == sizeof($instance['history'])-2)
                                        $delta = '20px';
                                    ?>
                                    <div class="year <?php echo $active; ?> <?php echo $last; ?>" data-year="<?php echo $value['year']; ?>" style="width: calc(<?php echo 100/(sizeof($instance['history'])-1) ?>% - <?php echo $delta; ?>)">
                                        <a class="bt"></a>
                                        <div class="text text13"><?php echo $value['year']; ?></div>
                                        <div class="point"></div>
                                    </div>
                                <?php
                                    $active = '';
                                }
                                ?>
                                <div class="clear"></div>
                                <div class="line"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="events">
                    <?php
                    $active = 'active';
                    foreach ($instance['history'] as $key => $value) { ?>
                        <div class="row event <?php echo $active; ?>" data-year="<?php echo $value['year']; ?>">
                            <div class="col-12 col-md-4">
                                <?php
                                if(strlen($value['photo']) > 0 && $value['photo'] != 0)
                                    echo '<img src="'. wp_get_attachment_image_src($value['photo'], 'large', true)[0] .'" />';
                                ?>
                            </div>
                            <div class="col-12 col-md-6 offset-md-1">
                                <div class="description text15"><?php echo $value['description']; ?></div>
                            </div>
                        </div>
                    <?php
                        $active = '';
                    }
                    ?>
                </div>

            </div>
        </div>
</div>
