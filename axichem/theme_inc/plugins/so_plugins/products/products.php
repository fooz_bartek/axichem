<?php

/*
Widget Name: Axichem
Description:
Author:
*/


class Products_Widget extends SiteOrigin_Widget {

	const SO_PLUGIN_SLUG = 'products';
	const SO_PLUGIN_DIR =  THEME_PLUGIN_DIRECTORY . 'so_plugins/products/';
	const SO_PLUGIN_URL =  THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/products/';
	const SO_PLUGIN_VER =  1;

	function __construct() {

		$form_options = array(
			'header' => array(
                'type' => 'text',
                'label' => __('Header', 'axichem'),
                'default' => ''
            ),
            'text' => array(
                'type' => 'tinymce',
			   	'label' => __('Text', 'axichem'),
 			  	'default' => '',
			   	'rows' => 5,
			   	'default_editor' => 'html',
			   	'button_filters' => array(
					'mce_buttons' => array($this, 'filter_mce_buttons'),
				   	'mce_buttons_2' => array($this, 'filter_mce_buttons_2'),
				   	'mce_buttons_3' => array($this, 'filter_mce_buttons_3'),
				   	'mce_buttons_4' => array($this, 'filter_mce_buttons_5'),
				   	'quicktags_settings' => array($this, 'filter_quicktags_settings'),
			   	),
            ),


			'products' => array(
				'type' => 'repeater',
		        'label' => __('Brands' , 'axichem'),
		        'item_name'  => __('Repeater item', 'siteorigin-widgets'),
		        'item_label' => array(
		        	'selector'     => "[id*='title']",
		         	'update_event' => 'change',
					'value_method' => 'val'
				),
		        'fields' => array(
		            'title' => array(
		                'type' => 'text',
		                'label' => __('Title', 'axichem' )
		            ),
					'photo' => array(
		                'type' => 'media',
		                'label' => __('Photo ', 'axichem'),
		                'choose' => __('Select', 'axichem'),
		                'update' => __('Set', 'axichem'),
		                'library' => 'image',
		                'fallback' => true
		            ),
					'url' => array(
					   'type' => 'link',
					   'label' => __('Url', 'axichem' )
				   )
		        )
		    )
		);
		add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

		parent::__construct(
				'products',
				__('Products list ', 'axichem'),
				array(
						'description' => __('description', 'axichem'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-admin-settings'
				),
				array(
				),
				$form_options,
				THEME_PLUGIN_DIRECTORY.'so_plugins/products/'
				);
		}

		function initialize(){
			wp_enqueue_style(self::SO_PLUGIN_SLUG, self::SO_PLUGIN_URL . 'tpl/style.css');
			//wp_enqueue_script(self::SO_PLUGIN_SLUG, self::SO_PLUGIN_URL . 'tpl/script.js', array('jquery'));
	    }
		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('Products Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/products/', 'Products_Widget');
?>
