<?php
$widget_name = "articleothers";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<div id="<?php echo $widget_id; ?>" class="widget widget-articleothers so-wrapper so-panel widget" data-animationcss="anim_opacity" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="header text15"><?php _e('Further reading', 'axichem') ?></div>
            </div>

            <?php
            $cat = wp_get_post_terms(get_the_ID(), 'type');
            $term = get_term($cat[0]->term_id, 'type');
            $tax_query = array();
            $tax_query[] = array(
                'taxonomy' => 'type',
                'field' => 'slug',
                'terms' => $cat[0]->slug
            );

            $queryFeed = new WP_Query(
                array(
                    'post_type' => 'news',
                    'posts_per_page' => 3,
                    'post__not_in' => array(get_the_ID()),
                    'tax_query' => $tax_query,
                    'order' => "DESC",
                    'orderby' => 'date',
                    'ignore_sticky_posts' => 1,
                    'post_status' => 'publish'
                )
            );
            ?>

            <?php
            $x = 1;
            while ($queryFeed->have_posts()) : $queryFeed->the_post();
            ?>

                <div class="col col-12 col-md-4">
                    <div class="box">
                        <div class="date text15"><?php echo __(get_the_date('F d, Y')); ?></div>
                        <a class="title text22" href="<?php echo get_permalink(); ?>">
                            <?php echo get_the_title(); ?>
                            <svg class="arrow" x="0px" y="0px" width="18.694px" height="15.225px" viewBox="0 0 18.694 15.225" enable-background="new 0 0 18.694 15.225" xml:space="preserve">
                            	<polyline fill="none" fill="none" stroke-miterlimit="10" points="10.669,14.23 17.606,7.013 10.869,0 	"/>
                            	<line fill="none" stroke-miterlimit="10" x1="17.799" y1="7.024" x2="0" y2="7.024"/>
                            </svg>
                        </a>
                    </div>
                </div>

            <?php endwhile; ?>

        </div>
    </div>
</div>
