<?php
$widget_name = "socialicon";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<div id="<?php echo $widget_id; ?>" class="widget widget-socialicon so-wrapper so-panel widget" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
    <a href="<?php echo $instance['url']; ?>" style="background-image: url(<?php echo wp_get_attachment_image_src($instance['icon'], 'full', true)[0]; ?>)">
    </a>
</div>
