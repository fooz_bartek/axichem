<?php

/*
Widget Name: Axichem
Description:
Author:
*/


class Block_TextAndPhoto_Widget extends SiteOrigin_Widget
{
    const SO_PLUGIN_SLUG = 'block_text_and_photo';
    const SO_PLUGIN_DIR =  THEME_PLUGIN_DIRECTORY . 'so_plugins/block_text_and_photo/';
    const SO_PLUGIN_URL =  THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/block_text_and_photo/';
    const SO_PLUGIN_VER =  1;

    public function __construct()
    {
        $form_options = array(
            'html'.(rand()*100) => array(
                'type' => 'html',
                'default' => '<h2>Options</h2><hr />'
            ),
            'side' => array(
                'type' => 'radio',
                'label' => __('Select side', 'axichem'),
                'default' => 'left',
                'options' => array(
                    'left' => __('Background | Text', 'axichem'),
                    'right' => __('Text | Background', 'axichem')
                )
            ),
            'side_width_left' => array(
                'type' => 'select',
                'label' => __('Width columns (left)', 'axichem'),
                'default' => '6',
                'options' => array(
                    '1' => __('1 / 12', 'axichem'),
                    '2' => __('2 / 12', 'axichem'),
                    '3' => __('3 / 12', 'axichem'),
                    '4' => __('4 / 12', 'axichem'),
                    '5' => __('5 / 12', 'axichem'),
                    '6' => __('6 / 12', 'axichem'),
                    '7' => __('7 / 12', 'axichem'),
                    '8' => __('8 / 12', 'axichem'),
                    '9' => __('9 / 12', 'axichem'),
                    '10' => __('10 / 12', 'axichem'),
                    '11' => __('11 / 12', 'axichem')
                )
            ),
            'side_width_right' => array(
                'type' => 'select',
                'label' => __('Width columns (right)', 'axichem'),
                'default' => '6',
                'options' => array(
                    '1' => __('1 / 12', 'axichem'),
                    '2' => __('2 / 12', 'axichem'),
                    '3' => __('3 / 12', 'axichem'),
                    '4' => __('4 / 12', 'axichem'),
                    '5' => __('5 / 12', 'axichem'),
                    '6' => __('6 / 12', 'axichem'),
                    '7' => __('7 / 12', 'axichem'),
                    '8' => __('8 / 12', 'axichem'),
                    '9' => __('9 / 12', 'axichem'),
                    '10' => __('10 / 12', 'axichem'),
                    '11' => __('11 / 12', 'axichem')
                )
            ),

            'html'.(rand()*100) => array(
                'type' => 'html',
                'default' => '<br /><br /><h2>Background</h2><hr />'
            ),
			'background' => array(
                'type' => 'media',
                'label' => __('Background - add photo ', 'axichem'),
                'choose' => __('Select', 'axichem'),
                'update' => __('Set', 'axichem'),
                'library' => 'image',
                'fallback' => true
            ),
            'background_scale' => array(
                'type' => 'checkbox',
                'label' => __('Background - fullscreen ', 'engine'),
                'default' => false
            ),
            'background_color' => array(
                'type' => 'color',
                'label' => __('Background - color', 'axichem'),
                'default' => '#ffffff'
            ),
            'background_align' => array(
                'type' => 'radio',
                'label' => __('Background - align photo', 'axichem'),
                'default' => 'center',
                'options' => array(
                    'left' => __('Left', 'axichem'),
                    'center' => __('Center', 'axichem'),
                    'right' => __('Right', 'axichem')
                )
            ),

            'background_marg' => array(
                'type' => 'inputmultiple',
                'real_label' => __('Background fullHD<br /> - margin top/bottom|Background Pc<br /> - margin top/bottom|Background Tablet<br /> - margin top/bottom|Background Phone<br /> - margin top/bottom', 'axichem'),
                'real_var' => __('padding_fullhd|padding_pc|padding_tablet|padding_phone', 'axichem'),
                'separate' => '|',
                'description' => __('Top Bottom, ex. "20 20"', 'axichem'),
                'default' => '0 0'
            ),



            'html'.(rand()*100) => array(
                'type' => 'html',
                'default' => '<br /><br /><h2>Text</h2><hr />'
            ),
            'inside_header' => array(
                'type' => 'text',
                'label' => __('Text - header', 'axichem'),
                'default' => ''
            ),
            'inside_text' => array(
                'type' => 'tinymce',
			   	'label' => __('Text - paragraph', 'axichem'),
 			  	'default' => '',
			   	'rows' => 5,
			   	'default_editor' => 'html',
			   	'button_filters' => array(
					'mce_buttons' => array($this, 'filter_mce_buttons'),
				   	'mce_buttons_2' => array($this, 'filter_mce_buttons_2'),
				   	'mce_buttons_3' => array($this, 'filter_mce_buttons_3'),
				   	'mce_buttons_4' => array($this, 'filter_mce_buttons_5'),
				   	'quicktags_settings' => array($this, 'filter_quicktags_settings'),
			   	),
            ),
            'inside_text_color' => array(
                'type' => 'color',
                'label' => __('Text - paragraph color', 'axichem'),
                'default' => ''
            ),
            'inside_some_link_text' => array(
                'type' => 'text',
                'label' => __('Link - text', 'axichem'),
                'default' => ''
            ),
            'inside_some_link_url' => array(
                'type' => 'link',
                'label' => __('Link - url', 'axichem'),
                'default' => ''
            ),


            'inside_pad' => array(
                'type' => 'inputmultiple',
                'real_label' => __('Text (fullhd) - padding top/bottom|Text (pc) - padding top/bottom|Text (tablet) - padding top/bottom|Text (phone) - padding top/bottom', 'axichem'),
                'real_var' => __('padding_fullhd|padding_pc|padding_tablet|padding_phone', 'axichem'),
                'description' => __('Top Bottom, ex. "20 20"', 'axichem'),
                'default' => '0 0'
            ),

        );
        add_filter('siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ));

        parent::__construct(
                'block_text_and_photo',
                __('Block: Text & Photo ', 'axichem'),
                array(
                        'description' => __('description', 'axichem'),
                        'help'        => TEMPL_OWNER_URL,
                        'panels_groups' => array(TEMPL_NAME),
                        'panels_icon' => 'dashicons dashicons-admin-settings'
                ),
                array(
                ),
                $form_options,
                THEME_PLUGIN_DIRECTORY.'so_plugins/block_text_and_photo/'
                );
    }

    public function initialize()
    {
        wp_enqueue_style(self::SO_PLUGIN_SLUG, self::SO_PLUGIN_URL . 'tpl/style.css');
        wp_enqueue_script(self::SO_PLUGIN_SLUG, self::SO_PLUGIN_URL . 'tpl/script.js', array('jquery'));
    }
    public function widget($args, $instance)
    {
        include('tpl/tpl.php');
    }
    public function get_template_name($instance)
    {
        return 'tpl';
    }

    public function get_style_name($instance)
    {
        return '';
    }

    public function filter_mce_buttons($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
                 ($key = array_search('dfw', $buttons)) !== false) {
            unset($buttons[$key]);
        }
        return $buttons;
    }
    public function filter_mce_buttons_2($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
                     ($key = array_search('dfw', $buttons)) !== false) {
            unset($buttons[$key]);
        }
        return $buttons;
    }
    public function filter_mce_buttons_3($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
                     ($key = array_search('dfw', $buttons)) !== false) {
            unset($buttons[$key]);
        }
        return $buttons;
    }
    public function filter_mce_buttons_4($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
                     ($key = array_search('dfw', $buttons)) !== false) {
            unset($buttons[$key]);
        }
        return $buttons;
    }
    public function quicktags_settings($settings, $editor_id)
    {
        $settings['buttons'] = preg_replace('/,fullscreen/', '', $settings['buttons']);
        $settings['buttons'] = preg_replace('/,dfw/', '', $settings['buttons']);
        return $settings;
    }
    public function sanitize_date($date_to_sanitize)
    {
        // Perform custom date sanitization here.
        $sanitized_date = sanitize_text_field($date_to_sanitize);
        return $sanitized_date;
    }
}
siteorigin_widget_register('Block_TextAndPhoto Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/block_text_and_photo/', 'Block_TextAndPhoto_Widget');
