var clickHandler = ('touchend' in document.documentElement ? "touchend" : "click");

jQuery(document).ready(function() {
    function blocktextphotoSize() {
        jQuery('.widget-blocktextphoto').each(function() {
            if (jQuery(this).find('.background').hasClass('fullscreen')) {

                if(jQuery(this).data('side') == 'left')
                    delta = 15;
                if(jQuery(this).data('side') == 'right')
                    delta = -15;

                bodyWidth = jQuery(window).width();
                containerWidth = jQuery(this).find('.container').outerWidth();
                colWidth = jQuery(this).find('.background').closest('.col').outerWidth();
                width = (bodyWidth - containerWidth) / 2 + colWidth + delta;

                jQuery(this).find('.background').width(width);
            }
        })
    }
    blocktextphotoSize();

    jQuery(window).resize(function() {
        blocktextphotoSize()
    })
});
