var clickHandler = ('touchend' in document.documentElement ? "touchend" : "click");

jQuery(document).ready(function() {
    function patentSize() {

        hArray = {};
        p = hMax = 0;

        jQuery('.widget-patents .patent').height('auto');
        jQuery('.widget-patents .patent').each(function () {
            if(p != jQuery(this).closest('.col').position().top)
                hArray[p] = hMax;

            p = jQuery(this).closest('.col').position().top;
            jQuery(this).attr('data-p', p);

            if(hMax < jQuery(this).outerHeight())
                hMax = jQuery(this).outerHeight();
        })
        hArray[p] = hMax;

        jQuery.each(hArray, function (keyFeature, valueFeature) {
            t = jQuery('.widget-patents .patent[data-p="'+ keyFeature +'"]');
            t.outerHeight(valueFeature)
		})
    }
    patentSize();
    setTimeout(function(){ patentSize(); }, 1000);

    jQuery(window).resize(function() {
        patentSize()
    })
});
