<?php
$widget_name = "patents";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];

$section_padDecode = json_decode($instance['section_pad'], true);
$section_padStyle = '{"fullhd":"padding:'. $section_padDecode['padding_fullhd'] .'","pc":"padding:'. $section_padDecode['padding_pc'] .'","tablet":"padding:'. $section_padDecode['padding_tablet'] .'","phone":"padding:'. $section_padDecode['padding_phone'] .'"}';
?>

<div id="<?php echo $widget_id; ?>" class="widget widget-patents so-wrapper so-panel widget" data-animationcss="anim_opacity" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" data-style='<?php echo $section_padStyle; ?>'>
    <div class="container">
        <div class="row patents">
            <?php
            $queryOfferAll = new WP_Query(
                array(
                    'post_type' => 'patents',
                    'posts_per_page' => -1,
                    'order' => "DESC",
                    'orderby' => 'menu_order',
                    'ignore_sticky_posts' => 1,
                    'post_status' => 'publish'
                )
            );
            while ($queryOfferAll->have_posts()) : $queryOfferAll->the_post();
                $term = wp_get_post_terms( get_the_ID(), 'patents-category' );

            ?>

                <div class="col col-12 col-md-6 col-lg-4">
                    <div class="patent" data-cat="<?php echo $term[0]->slug; ?>">
                        <div class="number text15"><?php echo get_field('number', get_the_ID()); ?></div>
                        <div class="title text22"><?php echo get_the_title(); ?></div>
                        <div class="desc text15"><?php echo get_the_content(); ?></div>
                        <div class="category text15"><?php echo $term[0]->name; ?></div>
                    </div>
                </div>

            <?php
            endwhile;
            wp_reset_query();
            ?>
        </div>
    </div>
</div>
