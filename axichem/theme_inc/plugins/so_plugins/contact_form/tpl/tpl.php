<?php
$widget_name = "contactform";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<div id="<?php echo $widget_id; ?>" class="widget widget-contactform so-wrapper so-panel widget" data-animationcss="anim_opacity" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-5">
                <div class="header text38"><?php echo $instance['header'];?></div>
            </div>
            <div class="col-12 col-md-7">
                <?php echo $instance['text'];?>
            </div>
        </div>
    </div>
</div>
