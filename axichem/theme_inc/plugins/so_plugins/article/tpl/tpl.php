<?php
$widget_name = "article";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<div id="<?php echo $widget_id; ?>" class="widget widget-article so-wrapper so-panel widget" data-animationcss="anim_opacity" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="date text15"><?php echo __(get_the_date('F d, Y')); ?></div>
                <div class="title text22"><?php echo get_the_title(); ?></div>

                <?php if(get_post_meta(get_the_ID(), 'FilesUrl', true) ){ ?>
                    <a class="download" href="<?php echo get_post_meta(get_the_ID(), 'FilesUrl', true ); ?>" target="_blank">
                        <svg x="0px" y="0px" width="24px" height="27px" viewBox="0 0 24 27" style="enable-background:new 0 0 24 27;" xml:space="preserve">
                            <line stroke-width="2" fill="none" x1="12" y1="0" x2="12" y2="18"></line>
                            <polyline stroke-width="1" fill="none" points="1,18 1,26 23,26 23,18"></polyline>
                            <polyline stroke-width="1" fill="none" points="6,11 12.5,17.5 18.7,11.3"></polyline>
                        </svg>
                    </a>
                <?php } ?>

                <a class="back left" href="javascript: history.go(-1)">
                    <svg x="0px" y="0px" width="18.7px" height="15.2px" viewBox="0 0 18.7 15.2" style="enable-background:new 0 0 18.7 15.2;" xml:space="preserve">
                        <polyline fill="none" points="8,0 1.1,7.2 7.8,14.2 	"/>
                        <line x1="0.9" y1="7.2" x2="18.7" y2="7.2"/>
                    </svg>
                    <span class="text text15"><?php _e('Back','axichem'); ?></span>
                </a>
            </div>
            <div class="col-12 col-md-7 offset-md-1">
                <div class="content text15"><?php echo get_the_content(); ?></div>
                <a class="back right" href="javascript: history.go(-1)">
                    <svg x="0px" y="0px" width="18.7px" height="15.2px" viewBox="0 0 18.7 15.2" style="enable-background:new 0 0 18.7 15.2;" xml:space="preserve">
                        <polyline fill="none" points="8,0 1.1,7.2 7.8,14.2 	"/>
                        <line x1="0.9" y1="7.2" x2="18.7" y2="7.2"/>
                    </svg>
                    <span class="text text15"><?php _e('Back','axichem'); ?></span>
                </a>
            </div>
        </div>
    </div>
</div>
