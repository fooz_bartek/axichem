<?php
$widget_name = "faq";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<div id="<?php echo $widget_id; ?>" class="widget widget-faq so-wrapper so-panel widget" data-animationcss="anim_opacity" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >

    <div class="container">
    <div class="row">
    <div class="col col-12">

    <div class="tabs_container">
        <div class="scroll">
            <?php
            $active = 'active';
            foreach ($instance['faq'] as $key => $value) {
                echo '<div class="tab text19 '. $active .'" data-key="'. $key .'">'. $value['header'] .'</div>';
                $active = '';
            }
            ?>
            <div class="clear"></div>
            <div class="line"></div>
        </div>
    </div>
    <div class="sections_container">
        <?php
        $activeGroup = $active = 'active';
        foreach ($instance['faq'] as $key => $value) {
            ?>

            <div class="sections_group <?php echo $activeGroup; ?>" data-key="<?php echo $key; ?>">

            <?php
            foreach ($value['issues'] as $keyIssues => $valueIssues) {
                ?>

                <div class="section <?php echo $active; ?>" data-section="<?php echo $keyIssues; ?>">
                    <div class="arrow">
                        <svg x="0px" y="0px" width="13.6px" height="7.5px" viewBox="0 0 13.6 7.5" style="enable-background:new 0 0 13.6 7.5;" xml:space="preserve">
                        	<polyline fill="none" stroke-width="2" points="0.4,7.2 6.8,0.7 13.3,7.2 	"/>
                        </svg>
                    </div>
                    <div class="short">
                        <div class="row">
                            <div class="col-12 col-md-12">
                                <div class="helper text22"><?php echo $valueIssues['topic']; ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="more">
                        <div class="row">
                            <div class="col-12 col-md-4">
                                <div class="topic text22"><?php echo $valueIssues['topic']; ?></div>
                            </div>
                            <div class="col-12 col-md-7">
                                <div class="description text15"><?php echo $valueIssues['description']; ?></div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php
                $active = '';
            }
            ?>

            </div>

            <?php
            $activeGroup = '';
        }
        ?>
    </div>

    </div>
    </div>
    </div>
</div>
