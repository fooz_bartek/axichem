<?php

/*
Widget Name: Axichem
Description:
Author:
*/


class Block_TextsAndBackground_Widget extends SiteOrigin_Widget
{
    const SO_PLUGIN_SLUG = 'block_texts_and_fullbackground';
    const SO_PLUGIN_DIR =  THEME_PLUGIN_DIRECTORY . 'so_plugins/block_texts_and_fullbackground/';
    const SO_PLUGIN_URL =  THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/block_texts_and_fullbackground/';
    const SO_PLUGIN_VER =  1;

    public function __construct()
    {
        $form_options = array(
            'html'.(rand()*100) => array(
                'type' => 'html',
                'default' => '<h2>Options</h2><hr />'
            ),
            'side_width' => array(
                'type' => 'select',
                'label' => __('Width columns ', 'axichem'),
                'default' => 'thing_1',
                'options' => array(
                    '1-11' => __('1 / 11', 'axichem'),
                    '2-10' => __('2 / 10', 'axichem'),
                    '3-9' => __('3 / 9', 'axichem'),
                    '4-8' => __('4 / 8', 'axichem'),
                    '5-7' => __('5 / 7', 'axichem'),
                    '6-6' => __('6 / 6', 'axichem'),
                    '7-5' => __('7 / 5', 'axichem'),
                    '8-4' => __('8 / 4', 'axichem'),
                    '9-3' => __('9 / 3', 'axichem'),
                    '10-2' => __('10 / 2', 'axichem'),
                    '11-1' => __('11 / 1', 'axichem')
                )
            ),
            'align_vertical' => array(
                'type' => 'checkbox',
                'label' => __( 'Center inside', 'axichem' ),
                'default' => false
            ),

            'html'.(rand()*100) => array(
                'type' => 'html',
                'default' => '<br /><br /><h2>Background</h2><hr />'
            ),
			'background' => array(
                'type' => 'media',
                'label' => __('Background - add photo ', 'axichem'),
                'choose' => __('Select', 'axichem'),
                'update' => __('Set', 'axichem'),
                'library' => 'image',
                'fallback' => true
            ),
            'background_color' => array(
                'type' => 'color',
                'label' => __('Background - color', 'axichem'),
                'default' => '#ffffff'
            ),
            'background_align' => array(
                'type' => 'radio',
                'label' => __('Background - align photo', 'axichem'),
                'default' => 'center',
                'options' => array(
                    'left' => __('Left', 'axichem'),
                    'center' => __('Center', 'axichem'),
                    'right' => __('Right', 'axichem')
                )
            ),


            'html'.(rand()*100) => array(
                'type' => 'html',
                'default' => '<br /><br /><h2>Left side</h2><hr />'
            ),
            'sideleft_pad' => array(
                'type' => 'inputmultiple',
                'real_label' => __('Section fullhd - padding top/bottom|Section - padding top/bottom|Section (tablet) - padding top/bottom|Section (phone) - padding top/bottom', 'axichem'),
                'real_var' => __('padding_fullhd|padding_pc|padding_tablet|padding_phone', 'axichem'),
                'description' => __('Top Bottom, ex. "20 20"', 'axichem'),
                'default' => '0 0'
            ),
            'sideleft_header' => array(
                'type' => 'text',
                'label' => __('Header', 'axichem'),
                'default' => ''
            ),
            'sideleft_text' => array(
                'type' => 'tinymce',
			   	'label' => __('Paragraph', 'axichem'),
 			  	'default' => '',
			   	'rows' => 5,
			   	'default_editor' => 'html',
			   	'button_filters' => array(
					'mce_buttons' => array($this, 'filter_mce_buttons'),
				   	'mce_buttons_2' => array($this, 'filter_mce_buttons_2'),
				   	'mce_buttons_3' => array($this, 'filter_mce_buttons_3'),
				   	'mce_buttons_4' => array($this, 'filter_mce_buttons_5'),
				   	'quicktags_settings' => array($this, 'filter_quicktags_settings'),
			   	),
            ),
            'sideleft_text_color' => array(
                'type' => 'color',
                'label' => __('Text - paragraph color', 'axichem'),
                'default' => ''
            ),
            'sideleft_text_pad' => array(
                'type' => 'inputmultiple',
                'real_label' => __('Text inside (fullhd) - padding top/bottom| Text inside - padding top/bottom|Text inside (tablet) - padding top/bottom|Text inside (phone) - padding top/bottom', 'axichem'),
                'real_var' => __('padding_fullhd|padding_pc|padding_tablet|padding_phone', 'axichem'),
                'description' => __('Top Bottom, ex. "20 20"', 'axichem'),
                'default' => '0 0'
            ),
            'sideleft_link_text' => array(
                'type' => 'text',
                'label' => __('Link - text', 'axichem'),
                'default' => ''
            ),
            'sideleft_link_url' => array(
                'type' => 'link',
                'label' => __('Link - url', 'axichem'),
                'default' => ''
            ),


            'html'.(rand()*100) => array(
                'type' => 'html',
                'default' => '<br /><br /><h2>Right side</h2><hr />'
            ),
            'sideright_pad' => array(
                'type' => 'inputmultiple',
                'real_label' => __('Section (fullhd) - padding top/bottom|Section - padding top/bottom|Section (tablet) - padding top/bottom|Section (phone) - padding top/bottom', 'axichem'),
                'real_var' => __('padding_fullhd|padding_pc|padding_tablet|padding_phone', 'axichem'),
                'description' => __('Top Bottom, ex. "20 20"', 'axichem'),
                'default' => '0 0'
            ),
            'sideright_header' => array(
                'type' => 'text',
                'label' => __('Text - header', 'axichem'),
                'default' => ''
            ),
            'sideright_text' => array(
                'type' => 'tinymce',
                'label' => __('Text - paragraph', 'axichem'),
                'default' => '',
                'rows' => 5,
                'default_editor' => 'html',
                'button_filters' => array(
                    'mce_buttons' => array($this, 'filter_mce_buttons'),
                    'mce_buttons_2' => array($this, 'filter_mce_buttons_2'),
                    'mce_buttons_3' => array($this, 'filter_mce_buttons_3'),
                    'mce_buttons_4' => array($this, 'filter_mce_buttons_5'),
                    'quicktags_settings' => array($this, 'filter_quicktags_settings'),
                ),
            ),
            'sideright_text_pad' => array(
                'type' => 'inputmultiple',
                'real_label' => __('Text inside (fullhd) - padding top/bottom|Text inside - padding top/bottom|Text inside (tablet) - padding top/bottom|Text inside (phone) - padding top/bottom', 'axichem'),
                'real_var' => __('padding_fullhd|padding_pc|padding_tablet|padding_phone', 'axichem'),
                'description' => __('Top Bottom, ex. "20 20"', 'axichem'),
                'default' => '0 0'
            ),
            'sideright_text_color' => array(
                'type' => 'color',
                'label' => __('Text - paragraph color', 'axichem'),
                'default' => ''
            ),
            'sideright_link_text' => array(
                'type' => 'text',
                'label' => __('Link - text', 'axichem'),
                'default' => ''
            ),
            'sideright_link_url' => array(
                'type' => 'link',
                'label' => __('Link - url', 'axichem'),
                'default' => ''
            ),



        );
        add_filter('siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ));

        parent::__construct(
                'block_texts_and_fullbackground',
                __('Block: Texts & Full Background ', 'axichem'),
                array(
                        'description' => __('description', 'axichem'),
                        'help'        => TEMPL_OWNER_URL,
                        'panels_groups' => array(TEMPL_NAME),
                        'panels_icon' => 'dashicons dashicons-admin-settings'
                ),
                array(
                ),
                $form_options,
                THEME_PLUGIN_DIRECTORY.'so_plugins/block_texts_and_fullbackground/'
                );
    }

    public function initialize()
    {
        wp_enqueue_style(self::SO_PLUGIN_SLUG, self::SO_PLUGIN_URL . 'tpl/style.css');
        wp_enqueue_script(self::SO_PLUGIN_SLUG, self::SO_PLUGIN_URL . 'tpl/script.js', array('jquery'));
    }
    public function widget($args, $instance)
    {
        include('tpl/tpl.php');
    }
    public function get_template_name($instance)
    {
        return 'tpl';
    }

    public function get_style_name($instance)
    {
        return '';
    }

    public function filter_mce_buttons($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
                 ($key = array_search('dfw', $buttons)) !== false) {
            unset($buttons[$key]);
        }
        return $buttons;
    }
    public function filter_mce_buttons_2($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
                     ($key = array_search('dfw', $buttons)) !== false) {
            unset($buttons[$key]);
        }
        return $buttons;
    }
    public function filter_mce_buttons_3($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
                     ($key = array_search('dfw', $buttons)) !== false) {
            unset($buttons[$key]);
        }
        return $buttons;
    }
    public function filter_mce_buttons_4($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
                     ($key = array_search('dfw', $buttons)) !== false) {
            unset($buttons[$key]);
        }
        return $buttons;
    }
    public function quicktags_settings($settings, $editor_id)
    {
        $settings['buttons'] = preg_replace('/,fullscreen/', '', $settings['buttons']);
        $settings['buttons'] = preg_replace('/,dfw/', '', $settings['buttons']);
        return $settings;
    }
    public function sanitize_date($date_to_sanitize)
    {
        // Perform custom date sanitization here.
        $sanitized_date = sanitize_text_field($date_to_sanitize);
        return $sanitized_date;
    }
}
siteorigin_widget_register('Block_TextsAndBackground Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/block_texts_and_fullbackground/', 'Block_TextsAndBackground_Widget');
