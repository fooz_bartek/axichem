<?php
$widget_name = "block_texts_and_fullbackground";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<div id="<?php echo $widget_id; ?>" class="widget widget-blocktextsbackground so-wrapper so-panel widget" data-animationcss="anim_opacity" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" data-side="<?php echo $instance['side']; ?>" data-center="<?php echo $instance['align_vertical']; ?>">
    <div class="container">
        <div class="row">
            <?php
            // Side setup
            // ================================

            $sidewidthArray = array(
                "1" => "col-12 col-md-1 col-lg-1",
                "2" => "col-12 col-md-1 col-lg-2",
                "3" => "col-12 col-md-2 col-lg-3",
                "4" => "col-12 col-md-3 col-lg-4",
                "5" => "col-12 col-md-4 col-lg-5",
                "6" => "col-12 col-md-5 col-lg-6",
                "7" => "col-12 col-md-6 col-lg-7",
                "8" => "col-12 col-md-8 col-lg-8",
                "9" => "col-12 col-md-9 col-lg-9",
                "10" => "col-12 col-md-10 col-lg-10",
                "11" => "col-12 col-md-11 col-lg-11",
                "12" => "col-12 col-md-12 col-lg-12",
            );
            $sidewidth = explode('-', $instance['side_width']);

            $side1class = $sidewidthArray[$sidewidth[0]];
            if(strlen($instance['sideright_text']) > 0)
                $side2class = $sidewidthArray[$sidewidth[1]-1].' offset-sm-1';

            // Column left
            // ================================

            $sideleft_padDecode = json_decode($instance['sideleft_pad'], true);
            $side1Style = '{"fullhd":"padding:'. $sideleft_padDecode['padding_fullhd'] .'","pc":"padding:'. $sideleft_padDecode['padding_pc'] .'","tablet":"padding:'. $sideleft_padDecode['padding_tablet'] .'","phone":"padding:'. $sideleft_padDecode['padding_phone'] .'"}';

            $side1content = '<div class="inside" ';
            $side1content .= "data-style='". $side1Style ."'>";
            $side1content .= '<div class="inside_relative">';

            $color = '';
            if($instance['sideleft_text_color'])
                $color = 'color: '. $instance['sideleft_text_color'] .';';

            if(strlen($instance['sideleft_header']) > 0)
                $side1content .= '<div class="header text15" style="'. $color .'">'. $instance['sideleft_header'] .'</div>';
            if(strlen($instance['sideleft_text']) > 0){

                $style1TextJson = json_decode($instance['sideleft_text_pad'], true);
                $style1TextStr = '{"fullhd":"padding:'. $style1TextJson['padding_fullhd'] .'","pc":"padding:'. $style1TextJson['padding_pc'] .'","tablet":"padding:'. $style1TextJson['padding_tablet'] .'","phone":"padding:'. $style1TextJson['padding_phone'] .'"}';
                $style1Text .= "data-style='". $style1TextStr ."'";

                $sideleft_text = $instance['sideleft_text'];
                $sideleft_text = preg_replace('/<h1>/i', '<h1 class="text38">', $sideleft_text);
                $sideleft_text = preg_replace('/<h2>/i', '<h2 class="text38">', $sideleft_text);
                $sideleft_text = preg_replace('/<h3>/i', '<h3 class="text38">', $sideleft_text);
                $sideleft_text = preg_replace('/<h4>/i', '<h4 class="text38">', $sideleft_text);
                $sideleft_text = preg_replace('/<h5>/i', '<h5 class="text38">', $sideleft_text);
                $sideleft_text = preg_replace('/<h6>/i', '<h6 class="text38">', $sideleft_text);
                $sideleft_text = preg_replace('/<p>/i', '<p class="text22">', $sideleft_text);

                $side1content .= '<div class="text" '. $style1Text .'><div style="'. $color .'">'. do_shortcode(nl2br($sideleft_text)) .'</div></div>';
            }
            if(strlen($instance['sideleft_link_text']) > 0 && strlen($instance['sideleft_link_url']) > 0){
                $href = '#';
                if(strlen($instance['sideleft_link_url']) > 0){
                    if(strpos($instance['sideleft_link_url'], 'post:') === false){
                        $href = $instance['sideleft_link_url'];
                    } else {
                        $hreflId = str_replace('post: ','', $instance['sideleft_link_url']);
                        $href = get_permalink($hreflId);
                    }
                }
                $side1content .= '<a class="link text15" href="'. $href .'">'. $instance['sideleft_link_text'] .'</a>';
            }

            // Column right
            // ================================

            $sideright_padDecode = json_decode($instance['sideright_pad'], true);
            $side2Style = '{"fullhd":"padding:'. $sideright_padDecode['padding_fullhd'] .'","pc":"padding:'. $sideright_padDecode['padding_pc'] .'","tablet":"padding:'. $sideright_padDecode['padding_tablet'] .'","phone":"padding:'. $sideright_padDecode['padding_phone'] .'"}';

            $side2content = '<div class="inside" ';
            $side2content .= "data-style='". $side2Style ."'>";
            $side2content .= '<div class="inside_relative">';

            if(strlen($instance['sideright_header']) > 0)
                $side2content .= '<div class="header text15">'. $instance['sideright_header'] .'</div>';
            if(strlen($instance['sideright_text']) > 0){
                $color = '';
                if($instance['sideright_text_color'])
                    $color = 'color: '. $instance['sideright_text_color'] .';';

                $style2TextJson = json_decode($instance['sideright_text_pad'], true);
                $style2TextStr = '{"fullhd":"padding:'. $style2TextJson['padding_fullhd'] .'","pc":"padding:'. $style2TextJson['padding_pc'] .'","tablet":"padding:'. $style2TextJson['padding_tablet'] .'","phone":"padding:'. $style2TextJson['padding_phone'] .'"}';
                $style2Text .= "data-style='". $style2TextStr ."'";

                //$sideright_text = nl2br($instance['sideright_text']);
                $sideright_text = $instance['sideright_text'];
                $sideright_text = preg_replace('/<h1>/i', '<h1 class="text38">', $sideright_text);
                $sideright_text = preg_replace('/<h2>/i', '<h2 class="text38">', $sideright_text);
                $sideright_text = preg_replace('/<h3>/i', '<h3 class="text38">', $sideright_text);
                $sideright_text = preg_replace('/<h4>/i', '<h4 class="text38">', $sideright_text);
                $sideright_text = preg_replace('/<h5>/i', '<h5 class="text38">', $sideright_text);
                $sideright_text = preg_replace('/<h6>/i', '<h6 class="text38">', $sideright_text);
                $sideright_text = preg_replace('/<p>/i', '<p class="text22">', $sideright_text);

                $side2content .= '<div class="text" '. $style2Text .'><div style="'. $color .'">'. do_shortcode($sideright_text) .'</div></div>';
            }
            if(strlen($instance['sideright_link_text']) > 0 && strlen($instance['sideright_link_url']) > 0){
                $href = '#';
                if(strlen($instance['sideright_link_url']) > 0){
                    if(strpos($instance['sideright_link_url'], 'post:') === false){
                        $href = $instance['sideright_link_url'];
                    } else {
                        $hreflId = str_replace('post: ','', $instance['sideright_link_url']);
                        $href = get_permalink($hreflId);
                    }
                }
                $side2content .= '<a class="link text15" href="'. $href .'">'. $instance['sideright_link_text'] .'</a>';
            }


            $side1content .= '</div></div>';
            $side2content .= '</div></div>';
            ?>

            <div class="col col-left <?php echo $side1class; ?>">
                <?php echo $side1content; ?>
            </div>
            <div class="col col-right <?php echo $side2class; ?>">
                <?php echo $side2content; ?>
            </div>
        </div>
    </div>
    <?php
    if(strlen($instance['background']) > 1)
        $background_image = 'background-image: url('. wp_get_attachment_image_src($instance['background'], 'large', true)[0] .'); background-position: '. $instance['background_align'] .' center; ';
    if(strlen($instance['background_color']) > 1)
        $background_color = 'background-color: '. $instance['background_color'];

    echo '<div class="background '. $fullscreen .'" style="'. $background_image .' '. $background_color . '"></div>';

    ?>
</div>
