var clickHandler = ('touchend' in document.documentElement ? "touchend" : "click");

jQuery(document).ready(function() {
    function keyfeatureSize() {
        hArray = {};
        p = hMax = 0;

        jQuery('.widget-keyfeatures .keyfeature .desc').height('auto');
        jQuery('.widget-keyfeatures .keyfeature').each(function () {
            if(p != jQuery(this).closest('.col').position().top)
                hArray[p] = hMax;

            p = jQuery(this).closest('.col').position().top;
            jQuery(this).attr('data-p', p);

            if(hMax < jQuery(this).find('.desc').outerHeight())
                hMax = jQuery(this).find('.desc').outerHeight();
        })
        hArray[p] = hMax;

        jQuery.each(hArray, function (keyFeature, valueFeature) {
            t = jQuery('.widget-keyfeatures .keyfeature[data-p="'+ keyFeature +'"] .desc');
            t.outerHeight(valueFeature)
		})
    }
    keyfeatureSize();
    setTimeout(function(){ keyfeatureSize(); }, 1000);

    jQuery(window).resize(function() {
        keyfeatureSize()
    })
});
