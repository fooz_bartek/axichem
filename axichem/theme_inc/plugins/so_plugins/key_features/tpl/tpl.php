<?php
$widget_name = "keyfeatures";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<div id="<?php echo $widget_id; ?>" class="widget widget-keyfeatures so-wrapper so-panel widget" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" style="background-color: <?php echo $instance['background_color']; ?>">
    <div class="container">
        <div class="row description">
            <div class="col-12 col-sm-6 col-md-5">
                <div class="header text15"><?php echo $instance['header']; ?></div>
            </div>
            <div class="col-12 col-sm-6 col-md-7">
                <div class="text text38"><?php echo $instance['text']; ?></div>
            </div>
        </div>

        <div class="row keyfeatures">
            <?php
            if(sizeof($instance['key_features']) > 0)
            foreach ($instance['key_features'] as $key => $value) {
                ?>

                <div class="col col-12 col-md-4">
                    <?php
                    $href = '#';
                    if(strlen($value['url']) > 0){
                        if(strpos($value['url'], 'post:') === false){
                            $href = $value['url'];
                        } else {
                            $hreflId = str_replace('post: ','', $value['url']);
                            $href = get_permalink($hreflId);
                        }
                    }
                    ?>

                    <?php
                    $tag = 'div';
                    $tagHref = '';
                    if(strlen($value['url']) > 0){
                        $tag = 'a';
                        $tagHref = 'href="'. $href .'"';
                    }
                    ?>
                    <<?php echo $tag; ?> <?php echo $tagHref; ?> class="keyfeature">
                        <div class="icon_container">
                            <img class="icon" src="<?php echo wp_get_attachment_image_src($value['photo'], 'full', true)[0]; ?>" />
                        </div>
                        <div class="desc">
                            <span class="text text22"><?php echo $value['text']; ?></span>
                            <?php if(strlen($value['url']) > 0){ ?>
                                <div class="arrow">
                                    <svg width="18.694px" height="15.225px" viewBox="0 0 18.694 15.225" enable-background="new 0 0 18.694 15.225" xml:space="preserve">
                                    	<polyline fill="none" stroke-miterlimit="10" points="10.669,14.23 17.606,7.013 10.869,0"/>
                                    	<line fill="none" stroke-miterlimit="10" x1="17.799" y1="7.024" x2="0" y2="7.024"/>
                                    </svg>
                                </div>
                            <?php } ?>
                        </div>
                    </<?php echo $tag; ?>>
                </div>

            <?php } ?>
        </div>
    </div>
</div>
