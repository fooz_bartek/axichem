var clickHandler = ('touchend' in document.documentElement ? "touchend" : "click");

jQuery(document).ready(function() {

    if (jQuery('.widget-contactmap').length == 1) {
        param = jQuery('#maps').data('param').split(',');

        function initialize() {
            var styles = [{
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#333739"
                    }]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#2ecc71"
                    }]
                },
                {
                    "featureType": "poi",
                    "stylers": [{
                            "color": "#2ecc71"
                        },
                        {
                            "lightness": -7
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [{
                            "color": "#2ecc71"
                        },
                        {
                            "lightness": -28
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [{
                            "color": "#2ecc71"
                        },
                        {
                            "visibility": "on"
                        },
                        {
                            "lightness": -15
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [{
                            "color": "#2ecc71"
                        },
                        {
                            "lightness": -18
                        }
                    ]
                },
                {
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#ffffff"
                    }]
                },
                {
                    "elementType": "labels.text.stroke",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [{
                            "color": "#2ecc71"
                        },
                        {
                            "lightness": -34
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry",
                    "stylers": [{
                            "visibility": "on"
                        },
                        {
                            "color": "#333739"
                        },
                        {
                            "weight": 0.8
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "stylers": [{
                        "color": "#2ecc71"
                    }]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                            "color": "#333739"
                        },
                        {
                            "weight": 0.3
                        },
                        {
                            "lightness": 10
                        }
                    ]
                }
            ];

            myLatLng = new google.maps.LatLng(param[0], param[1])
            myOptions = {
                    mapTypeControlOptions: {
                        mapTypeIds: ['Styled']
                    },
                    zoom: Number(param[2]),
                    center: myLatLng,
                    disableDefaultUI: true,
                    streetViewControl: false,
                    zoomControl: true,
                    zoomControlOptions: {
                        position: google.maps.ControlPosition.LEFT_BOTTOM
                    },

                    mapTypeId: 'Styled'
                },
                map = new google.maps.Map(document.getElementById('maps'), myOptions),

                styledMapType = new google.maps.StyledMapType(styles, {
                    name: 'Styled'
                });
            map.mapTypes.set('Styled', styledMapType);

            marker = new google.maps.Marker({
                map: map,
                draggable: false,
                flat: true,
                position: myLatLng,
                icon: domainTemplate + '/img/pin.svg',
            });

            marker.setMap(map);
            moveMarker(map, marker);
        }

        function moveMarker(map, marker) {
            setTimeout(function() {
                marker.setPosition(myLatLng);
                map.panTo(myLatLng);
            }, 1500);
        };
        initialize();
    }
});
