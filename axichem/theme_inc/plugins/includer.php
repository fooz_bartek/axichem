<?php
// Lista wtyczek

if(class_exists('SiteOrigin_Widget')){



	function fpweb_exclude_icon_families_filter( $families ){

	 // if( isset( $families['fontawesome'] ) unset( $families['fontawesome'] );
		if( isset( $families['icomoon'] ) ) unset( $families['icomoon'] );
		if( isset( $families['genericons'] ) ) unset( $families['genericons'] );
		if( isset( $families['typicons'] ) ) unset( $families['typicons'] );
		if( isset( $families['elegantline'] ) ) unset( $families['elegantline'] );

		return $families;

	}
	add_filter( 'siteorigin_widgets_icon_families', 'fpweb_exclude_icon_families_filter' );


	// SO Plugin adds
	function fpweb_mytheme_add_widget_tabs($tabs) {
		$tabs[] = array(
			'title' => __(TEMPL_AUTHOR, 'engine'),
			'filter' => array(
				'groups' => array(TEMPL_NAME)
			)
		);

		return $tabs;
	}
	add_filter('siteorigin_panels_widget_dialog_tabs', 'fpweb_mytheme_add_widget_tabs', 20);

	// Dodanie opcji do rzędu w postaci chekboxa
	//https://siteorigin.com/docs/page-builder/hooks/custom-row-settings/
	function custom_row_style_fields($fields) {

		$fields['box_height'] = array(
			'name'        => __('Section height', 'engine'),
			'type'        => 'measurement',
			'group'       => 'attributes',
			'description' => __('Set minimal height of element', 'engine'),
			'priority'    => 4,
		);

		$fields['background_repeat'] = array(
			'name'        => __('No repeat bg', 'engine'),
			'type'        => 'checkbox',
			'group'       => 'design',
			'description' => __('Set br repeat', 'engine'),
			'priority'    => 8,
		);
		$fields['background_position'] = array(
			'name'        => __('Bg postion', 'engine'),
			'type'        => 'select',
			'group'       => 'design',
			'options' => array(
				'bg-center' => __( 'center', 'engine' ),
				'parallax' => __( 'brak dla parallaxy', 'engine' ),
				'bg-l_u' => __( 'left up', 'engine' ),
				'bg-l_d' => __( 'left down', 'engine' ),
				'bg-p_u' => __( 'right up', 'engine' ),
				'bg-p_d' => __( 'right down', 'engine' ),
			),
			'priority'    => 8,
		);


		return $fields;
	}
	add_filter( 'siteorigin_panels_row_style_fields', 'custom_row_style_fields' );

	function custom_row_style_attributes( $attributes, $args ) {

		if( !empty( $args['box_height'] ) ) {
			$attributes['style'] .= 'height:'.$args['box_height'].';';
		}
		if( $args['background_repeat'] ) {
			array_push($attributes['class'], 'background_norepeat');
		}else{
			array_push($attributes['class'], 'background_repeat');
		}
		if( !empty( $args['background_position'] ) ) {
			array_push($attributes['class'], $args['background_position'] );
		}

		return $attributes;
	}
	add_filter('siteorigin_panels_row_style_attributes', 'custom_row_style_attributes', 10, 2);


	// Register Widgets
	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/social_icon/social_icon.php');
	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/footer_ourbrands/footer_ourbrands.php');

	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/top/top.php');
	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/top_article/top_article.php');
	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/block_text_and_photo/block_text_and_photo.php');
	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/block_texts_and_fullbackground/block_texts_and_fullbackground.php');
	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/block_texts_and_backgrounds/block_texts_and_backgrounds.php');




	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/products/products.php');
	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/key_features/key_features.php');
	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/contact_map/contact_map.php');
	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/contact_form/contact_form.php');
	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/subscribe/subscribe.php');
	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/patents/patents.php');
	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/history/history.php');
	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/people/people.php');


	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/article/article.php');
	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/article_others/article_others.php');

	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/pages/pages.php');

	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/text/text.php');
	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/search/search.php');
	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/faq/faq.php');
	require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/404/404.php');




} // end iff siteorigin activated


// add numeric pagination
require_once(THEME_PLUGIN_DIRECTORY.'pagination/pagination.php');

?>
