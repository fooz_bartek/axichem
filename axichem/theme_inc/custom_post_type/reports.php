<?php
// Scientific Reports

function register_reports_init() {
    $args = array(
        'public' => true,
        'label'  => 'Scientific Reports',
        'rewrite' => array('slug' => 'reports'),
        'menu_icon'  => 'dashicons-megaphone',
        'supports' => array('title','editor','excerpt','thumbnail')
    );
    register_post_type('reports', $args );
}
add_action( 'init', 'register_reports_init' );
