<?php
// FEED SV

function register_cisionfeed_init() {
    $args = array(
        'public' => true,
        'label'  => 'Feed (cision)',
        'rewrite' => array('slug' => 'news'),
        'menu_icon'  => 'dashicons-megaphone',
        'supports' => array('title','editor','excerpt','thumbnail')
    );
    register_post_type('news', $args );
}
add_action( 'init', 'register_cisionfeed_init' );

register_taxonomy('type', 'news',
    array(
        'hierarchical' => true,
        'label' => __('Category'),
        'rewrite' => array('slug' => 'feed-category'),
        'hierarchical' => true,
        'rewrite' => true
    )
);


// FEED EN

// function register_cisionfeeden_init() {
//     $args = array(
//         'public' => true,
//         'label'  => 'Feed EN (cision)',
//         'rewrite' => array('slug' => 'news_en'),
//         'menu_icon'  => 'dashicons-megaphone',
//         'supports' => array('title','editor','excerpt','thumbnail')
//     );
//     register_post_type('news_en', $args );
// }
// add_action( 'init', 'register_cisionfeeden_init' );
//
// register_taxonomy('type-en', 'news_en',
//     array(
//         'hierarchical' => true,
//         'label' => __('Category'),
//         'rewrite' => array('slug' => 'feed-category-en'),
//         'hierarchical' => true,
//         'rewrite' => true
//     )
// );
