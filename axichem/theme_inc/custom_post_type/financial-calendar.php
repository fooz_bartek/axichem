<?php
// financial-calendar

function register_financialcalendar_init() {
    $args = array(
        'public' => true,
        'label'  => 'Financial Calendar',
        'rewrite' => array('slug' => 'financial-calendar'),
        'menu_icon'  => 'dashicons-megaphone',
        'supports' => array('title','editor','excerpt','thumbnail')
    );
    register_post_type('financial-calendar', $args );
}
add_action( 'init', 'register_financialcalendar_init' );
