<?php
/*
    Template Name: Page
*/
get_header();
?>

<?php
$actId = get_the_ID();
$page = get_post(get_the_ID());
$parentId = get_the_ID();
$parentTitle = $childrenTitle = $page->post_title;
$childrenContent = $page->post_content;

if($page->post_parent != 0){
    $page = get_post($page->post_parent);
    $parentId = $page->ID;
    $parentTitle = $page->post_title;
}
?>

    <div class="shortcode_pagesdisplay">
        <div class="top">
            <div class="container">
                <div class="row">
                    <div class="col col-12">
                        <div class="header text38"><?php echo $parentTitle; ?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pages">
            <div class="content text15">
                <?php
                $content = preg_replace('/<h1>/i', '<h1 class="text38">', $childrenContent);
                $content = preg_replace('/<h2>/i', '<h2 class="text38">', $content);
                $content = preg_replace('/<h3>/i', '<h3 class="text38">', $content);
                $content = preg_replace('/<h4>/i', '<h4 class="text32">', $content);
                $content = preg_replace('/<h5>/i', '<h5 class="text32">', $content);
                $content = preg_replace('/<h6>/i', '<h6 class="text32">', $content);
                echo do_shortcode($content);
                ?>
            </div>
        </div>
    </div>

<?php
get_footer();
?>
