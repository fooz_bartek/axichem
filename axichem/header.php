<?php
require_once 'Mobile_Detect.php';
$detect = new Mobile_Detect;

global $deviceType;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');

if ($detect->isMobile()) {
    $mobile = 'mobile';
}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php

        global $page, $paged;

        wp_title('|', true, 'right');

        // Add the blog name.
        bloginfo('name');

        // Add the blog description for the home/front page.
        $site_description = get_bloginfo('description', 'display');
        if ($site_description && (is_home() || is_front_page())) {
            echo " | $site_description";
        }


        ?></title>

    <link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_url'); ?>/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_url'); ?>/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_url'); ?>/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_url'); ?>/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_url'); ?>/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_url'); ?>/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_url'); ?>/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_url'); ?>/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url'); ?>/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo('template_url'); ?>/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url'); ?>/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('template_url'); ?>/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_url'); ?>/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php bloginfo('template_url'); ?>/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php bloginfo('template_url'); ?>/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="<?php echo TEMPL_DIR; ?>css/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <?php wp_head(); ?>

    <script type="text/javascript">
        var domainHome = "<?php bloginfo('home'); ?>";
        var domainTemplate = "<?php bloginfo('template_url'); ?>";
    </script>

    <!--[if lt IE 9]>
    <script src="<?php echo TEMPL_DIR; ?>/js/html5shiv.min.js" type="text/javascript"></script>
    <script src="<?php echo TEMPL_DIR; ?>/js/respond.min.js" type="text/javascript"></script>
    <![endif]-->

</head>

<body data-deviceglobal="<?php echo $deviceType; ?>" data-device="<?php echo $deviceType; ?>" <?php body_class(' height_100 '); ?>>
    <div id="loader" class="show">
        <img class="logo" src="<?php bloginfo('template_url'); ?>/img/logo.svg" />
        <div class="boxs">
            <div class="box box1"></div>
            <div class="box box2"></div>
            <div class="box box3"></div>
            <div class="box box4"></div>
        </div>
    </div>
    <header id="header">
        <div class="less">
            <div class="_off"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <a class="logo" href="<?php bloginfo('home'); ?>">
                            <img src="<?php bloginfo('template_url'); ?>/img/logo.svg" />
                        </a>
                        <button class="c-hamburger c-hamburger--htx">
                            <span>toggle menu</span>
                        </button>
                        <div class="extra">
                            <a class="bt-search" href="<?php bloginfo('home'); ?>?s=">
                                <svg width="17.443px" height="15px" viewBox="0 0 17.443 17.375" enable-background="new 0 0 17.443 17.375" xml:space="preserve">
                                <path d="M11.686,11l-0.277-0.313c2.333-2.718,2.021-6.835-0.698-9.168C7.992-0.815,3.897-0.514,1.564,2.204
                                	c-2.333,2.719-2.021,6.807,0.698,9.14c2.43,2.086,6.029,2.083,8.458-0.002L11,11.618v0.787l4.971,4.971l1.477-1.433L12.473,11
                                	H11.686z M6.507,11c-0.004,0-0.009,0-0.013,0c-2.476,0-4.483-2.052-4.483-4.528s2.007-4.506,4.483-4.506
                                	c2.477,0,4.484,2.041,4.484,4.518C10.982,8.956,8.98,11,6.507,11z"/>
                                </svg>
                            </a>
                            <div class="bt-language">
                                <?php
                                $languages = icl_get_languages('skip_missing=1');
                                foreach ($languages as $key => $value) {
                                    if($value['active'] == 0){
                                        $lg = $value['language_code'];
                                        $href = $value['url'];
                                        break;
                                    }
                                }
                                ?>
                                <a href="<?php echo $href; ?>">
                                    <span><?php echo $lg; ?></span>
                                </a>
                            </div>
                            <a class="link" href="<?php echo get_permalink(icl_object_id(765)); ?>"><?php echo get_the_title(icl_object_id(765)); ?></a>
                        </div>
                        <div class="links">
                            <?php
                            $actId = get_the_ID();

                            $menu = array();
                            $menuOrg = wp_get_nav_menu_items('Menu');

                            function findId($key, $menuOrg, $parent, $actId){
                                foreach ($menuOrg as $item) {
                                    if ($item->ID == $key) {
                                        $url = get_permalink($item->object_id);
                                        if ($item->type == 'custom') {
                                            $url = $item->url;
                                        }
                                        if (strpos($url, '#') !== false) {
                                            foreach ($menuOrg as $itemParent) {
                                                if ($itemParent->ID == $parent)
                                                    $url = get_permalink($itemParent->object_id) . $item->url;
                                            }
                                        }

                                        $active = '';
                                        if($actId == $item->object_id){
                                            $active = 'active';
                                        }

                                        $getpost = get_post($item->object_id);
                                        return '<a href="'. $url .'" class="'. $item->classes[0] .' '. $active .'" data-id="'. $item->object_id .'">'. $getpost->post_title. '</a>';
                                        break;
                                    }
                                }
                            }
                            ?>
                            <?php
                            // Build structure menu
                            // ========================================================
                            foreach ($menuOrg as $item) {
                                $href = $item->url;

                                if ($item->menu_item_parent == 0) {
                                    $menu[$item->ID] = $item->title;
                                }
                                if ($item->menu_item_parent != 0) {
                                    if (!is_array($menu[$item->menu_item_parent])) {
                                        $menu[$item->menu_item_parent] = array();
                                    }
                                    $menu[$item->menu_item_parent][$item->ID] = '';
                                }
                            }
                             //

                            // Display menu
                            // ========================================================
                            ?>
                            <ul id="menu-less">
                            <?php
                            foreach ($menu as $key => $item) {
                                if (sizeof($item) > 1) {
                                    echo '<li class="dropdown" data-id="'. $key .'">';
                                    echo findId($key, $menuOrg, '', $actId);
                                } else {
                                    echo '<li class="dropdown_none">';
                                    echo findId($key, $menuOrg, '', $actId);
                                }
                                echo '</li>';
                            }
                            ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="more">
            <div class="mask"></div>
            <div class="off"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="search">
                            <form role="search" method="get" id="searchform" action="<?php bloginfo('home'); ?>">
                                <span class="container-input">
                                    <input type="text" name="s" placeholder="<?php echo $_GET['s']; ?>" />
                                    <div class="line"></div>
                                </span>
                                <button type="submit">
                                    <svg width="17.443px" height="30px" viewBox="0 0 17.443 30" enable-background="new 0 0 17.443 30" xml:space="preserve">
                                    <path d="M11.686,17l-0.277-0.444c2.333-2.718,2.021-6.901-0.698-9.234C7.992,4.988,3.897,5.257,1.564,7.975
                                    	c-2.333,2.719-2.021,6.791,0.698,9.124c2.43,2.086,6.029,2.075,8.458-0.011L11,17.355v0.787l4.971,4.971l1.477-1.302L12.473,17
                                    	H11.686z M6.507,17c-0.004,0-0.009,0-0.013,0c-2.476,0-4.483-2.183-4.483-4.66c0-2.476,2.007-4.572,4.483-4.572
                                    	c2.477,0,4.484,2.14,4.484,4.616C10.982,14.857,8.98,17,6.507,17z"/>
                                    </svg>
                                </button>
                            </form>
                        </div>
                        <div class="links">
                            <ul id="menu-more">
                            <?php
                            foreach ($menu as $key => $item) {
                                if (sizeof($item) > 1) {
                                    echo '<li class="dropdown" data-id="'. $key .'">';
                                    echo findId($key, $menuOrg, '', $actId);
                                } else {
                                    echo '<li class="dropdown_none">';
                                    echo findId($key, $menuOrg, '', $actId);
                                }

                                if (sizeof($item) > 1) {
                                    echo '<ul>';
                                    echo '<li>';
                                    foreach ($item as $key2 => $item2) {
                                        echo findId($key2, $menuOrg, $key, $actId);
                                    }
                                    echo '<div class="clear"></div></li>';
                                    echo '</ul>';
                                }
                            echo '</li>';
                            }
                            ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="socials">
                    <?php
                    if (is_active_sidebar('social-sidebar'))
                        dynamic_sidebar('social-sidebar');
                    ?>
                </div>
            </div>
        </div>
    </header>
