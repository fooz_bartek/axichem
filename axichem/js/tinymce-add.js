jQuery( document ).on( 'tinymce-editor-init', function( event, editor ) {
    tinymce.activeEditor.formatter.register('paragraph-1', {
        block : 'div',
        classes : 'text18 textSpecial textGray textParagraph'
    });
    tinymce.activeEditor.formatter.register('paragraph-2', {
        block : 'div',
        classes : 'text15 textSpecial textBlack textParagraph'
    });
    tinymce.activeEditor.formatter.register('paragraph-3', {
        block : 'div',
        classes : 'textPressuraMono textBold textParagraph'
    });
    tinymce.activeEditor.formatter.register('paragraph-4', {
        block : 'div',
        classes : 'text22 textSpecial textBlack textBold textParagraph'
    });

    tinymce.activeEditor.formatter.register('paragraph-5', {
        block : 'div',
        classes : 'text24 textSpecial textBold textParagraph'
    });
    tinymce.activeEditor.formatter.register('paragraph-6', {
        block : 'div',
        classes : 'text38 textSpecial textBold textParagraph'
    });

});
