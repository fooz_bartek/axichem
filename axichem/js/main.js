var clickHandler = ('touchend' in document.documentElement ? "touchend" : "click");

var $ = jQuery.noConflict();
var $window = jQuery(window);

var rtime;
var timeout = false;
var delta = 200;
var parallaxrunset = 0;

jQuery(window).resize(function() {
    rtime = new Date();
    if (timeout === false) {
        timeout = true;
        setTimeout(resizeend, delta);
    }
});

function resizeend() {
    if (new Date() - rtime < delta) {
        setTimeout(resizeend, delta);
    } else {
        timeout = false;
        height_100();
        roweqheight();
        fullWidthElement();
        tabsheight();
    }
}

jQuery(document).ready(function() {

    jQuery('.niceselect').niceSelect();

    // General
    // ==================================================

    jQuery(window).load(function() {
        setTimeout(function() {
            if (window.location.hash.length > 0) {
                if ($('a[rel="' + window.location.hash.substring(1, 999) + '"]').length == 1) {
                    $('html, body').animate({
                        scrollTop: $('a[rel="' + window.location.hash.substring(1, 999) + '"]').position().top - $('#header .less').height()
                    }, 1000);
                }
            }
        }, 1000);
    })

    $(document).on(clickHandler, 'a', function(event) {
        if ($(this).attr('href'))
            if ($(this).attr('href').search('#') != -1) {
                setTimeout(function() {
                    if (window.location.hash.length > 0) {
                        if ($('a[rel="' + window.location.hash.substring(1, 999) + '"]').length == 1) {
                            $('html, body').animate({
                                scrollTop: $('a[rel="' + window.location.hash.substring(1, 999) + '"]').position().top - $('#header .less').height()
                            }, 1000);
                        }

                    }
                }, 100);
            }
    })

    // Render
    // ==================================================

    function pageRender() {
        device = 'computer';
		if ($(window).width() < 992)
			device = 'tablet';
		if ($(window).width() < 767)
			device = 'phone';

        $('body').attr('data-device', device);
    }
    pageRender();

    // Menu
    // ==================================================

    function menuRender() {
        aCountMax = 0;
        $('#header .more .links li.dropdown').each(function() {
            id = $(this).data('id');
            wMax = $(this).find('a').width();
            aCount = 0;
            if (!$(this).find('a').hasClass('width_auto')) {
                $(this).find('li a').each(function() {
                    aCount++
                    if (wMax < $(this).width())
                        wMax = $(this).width();
                })
                $('#header .links li.dropdown[data-id="' + id + '"]').width(wMax);

                if (aCount > aCountMax) {
                    aCountMax = aCount;
                }
            }
        })
        $('#header .more').height(aCountMax * $('#header .more .links li li a').height())
        $('#header .more .container').height(aCountMax * $('#header .more .links li li a').height())

        $('#header .more .links ul > li.dropdown a').each(function() {
            if ($(this).hasClass('active')) {
                $(this).closest('li').addClass('active');
            }
        })
    }
    menuRender();

    $(document).on('mouseenter', '#header .less .links a, #header .bt-search', function(event) {
        id = $(this).closest('li.dropdown').attr('data-id');
        $('#header').addClass('hover');

        $('#header .more .links ul li.dropdown').removeClass('hover');
        $('#header .more .links ul li.dropdown[data-id="' + id + '"]').addClass('hover');

        $('#header .more .search .container-input input[type="text"]').focus();
    })
    $(document).on('mouseenter', '#header.hover .more', function(event) {
        $('#header ._off').show();
    })



    $(document).on('mouseenter', '#header ._off', function(event) {
        $('#header').removeClass('hover');
        $('#header ._off').hide();
    })

    $(document).on('mouseleave', '#header', function(event) {
        $('#header').removeClass('hover');
        $('#header ._off').hide();
    })

    $(document).on('touchstart', '#content', function(event) {
        $('#header').removeClass('is-active');
        $('body').removeClass('frozen');
        $('#header').removeClass('hover');
        $('#header ._off').hide();
    })


    $(document).on(clickHandler, '#header .c-hamburger', function(event) {
        $('#header').toggleClass('is-active');
        $('body').toggleClass('frozen');
    })





    // more - white

    $(document).on('mouseenter', 'header .more .links ul li.dropdown', function(event) {
        id = $(this).attr('data-id');
        $(this).addClass('hover')
    })
    $(document).on('mouseleave', 'header .more .links ul li.dropdown', function(event) {
        $(this).removeClass('hover')
    })








    function sectionRender() {
        wWindow = $(window).width();

        $('.widget, .widget *').each(function() {
            style = $(this).data('style');

            if (typeof(style) != 'undefined') {
                styleAdd = '';

                if (wWindow <= 767) {
                    if (typeof(style.phone) != 'undefined') {
                        styleArray = style.phone.split(':');
                        if (styleArray[0] == 'padding') {
                            valueArray = styleArray[1].split(' ');
                            if (!$(this).hasClass('background')) {
                                styleAdd += 'padding: ' + valueArray[0] + 'px 0 ' + valueArray[1] + 'px 0;';
                            } else {
                                styleAdd += 'height: calc(100% - ' + (Number(valueArray[0]) + Number(valueArray[1])) + 'px); top: ' + valueArray[0] + 'px;';
                            }
                        }
                        if (styleArray[0] == 'margin') {
                            valueArray = styleArray[1].split(' ');
                            styleAdd += 'margin: ' + valueArray[0] + 'px 0 ' + valueArray[1] + 'px 0;';
                        }
                    }
                }
                if (wWindow > 767 && wWindow <= 1199) {
                    if (typeof(style.tablet) != 'undefined') {
                        styleArray = style.tablet.split(':');
                        if (styleArray[0] == 'padding') {
                            valueArray = styleArray[1].split(' ');
                            if (!$(this).hasClass('background')) {
                                styleAdd += 'padding: ' + valueArray[0] + 'px 0 ' + valueArray[1] + 'px 0;';
                            } else {
                                styleAdd += 'height: calc(100% - ' + (Number(valueArray[0]) + Number(valueArray[1])) + 'px); top: ' + valueArray[0] + 'px;';
                            }
                        }
                        if (styleArray[0] == 'margin') {
                            valueArray = styleArray[1].split(' ');
                            styleAdd += 'margin: ' + valueArray[0] + 'px 0 ' + valueArray[1] + 'px 0;';
                        }
                    }
                }
                if (wWindow > 1199 && wWindow <= 1600) {
                    if (typeof(style.pc) != 'undefined') {
                        styleArray = style.pc.split(':');
                        if (styleArray[0] == 'padding') {
                            valueArray = styleArray[1].split(' ');
                            if (!$(this).hasClass('background')) {
                                styleAdd += 'padding: ' + valueArray[0] + 'px 0 ' + valueArray[1] + 'px 0;';
                            } else {
                                styleAdd += 'height: calc(100% - ' + (Number(valueArray[0]) + Number(valueArray[1])) + 'px); top: ' + valueArray[0] + 'px;';
                            }
                        }
                        if (styleArray[0] == 'margin') {
                            valueArray = styleArray[1].split(' ');
                            styleAdd += 'margin: ' + valueArray[0] + 'px 0 ' + valueArray[1] + 'px 0;';
                        }
                    }
                }
                if (wWindow > 1600) {
                    if (typeof(style.fullhd) != 'undefined') {
                        styleArray = style.fullhd.split(':');
                        if (styleArray[0] == 'padding') {
                            valueArray = styleArray[1].split(' ');
                            if (!$(this).hasClass('background')) {
                                styleAdd += 'padding: ' + valueArray[0] + 'px 0 ' + valueArray[1] + 'px 0;';
                            } else {
                                styleAdd += 'height: calc(100% - ' + (Number(valueArray[0]) + Number(valueArray[1])) + 'px); top: ' + valueArray[0] + 'px;';
                            }
                        }
                        if (styleArray[0] == 'margin') {
                            valueArray = styleArray[1].split(' ');
                            styleAdd += 'margin: ' + valueArray[0] + 'px 0 ' + valueArray[1] + 'px 0;';
                        }
                    }
                }

                if (typeof(style.other) != 'undefined')
                    if (style.other.length > 1) {
                        styleAdd += style.other;
                    }

                $(this).attr('style', styleAdd);
            }

        })
    }
    sectionRender();






    $(window).resize(function() {
        menuRender();
        pageRender();
        sectionRender();
    });


    // SCroll
    // ===========================================================================

    jQuery('.shortcode_scroll').each(function() {
        var shortcode = $(this);
        var shortcode_arrow = '.' + $(this).attr('class').replace(/ /g, '.') + ' .arrows  a:not(.off)';

        function shortcode_scrollSetHeight() {
            p = shortcode.find('.items_container').attr('data-p')

            if ($(window).width() > 767) {
                hMax = 0;
                shortcode.find('.items_container .item').each(function() {
                    hh = jQuery(this).height();
                    if (hh > hMax)
                        hMax = hh
                })
                shortcode.find('.items_container').height(hMax);
            } else {
                hMax = shortcode.find('.items_container .item').eq(p).height();
                shortcode.find('.items_container').height(hMax);
            }

            pp = shortcode.find('.items .item').eq(p).position().top
            shortcode.find('.items').css({
                'top': -pp
            })

            shortcode.find('.items .item').removeClass('show');
            shortcode.find('.items .item').eq(p).addClass('show');
        }
        shortcode_scrollSetHeight();
        setTimeout(function() {
            shortcode_scrollSetHeight();
        }, 2000);


        function shortcode_scrollSetArrow() {
            shortcode.find('.navigation a').removeClass('off');

            p = pOld = shortcode.find('.items_container').attr('data-p');
            pMax = -1;
            shortcode.find('.items_container .item').each(function() {
                if (jQuery(this).html().length > 1)
                    pMax++;
            })
            if (p == 0) {
                p = 0;
                shortcode.find('.navigation .prev').addClass('off');
            }
            if (p == pMax) {
                shortcode.find('.navigation .next').addClass('off');
            }
        }


        $(window).resize(function() {
            shortcode_scrollSetHeight();
            shortcode_scrollSetArrow();
        });

    })

    $(document).on(clickHandler, '.shortcode_scroll .arrows  a:not(.off)', function(event) {
        shortcodeone = $(this).closest('.shortcode_scroll');
        shortcodeone.find('.navigation a').removeClass('off');

        p = pOld = shortcodeone.find('.items_container').attr('data-p');
        pMax = -1;
        shortcodeone.find('.items_container .item').each(function() {
            if (jQuery(this).html().length > 1)
                pMax++;
        })

        if ($(this).hasClass('prev'))
            p--;
        if ($(this).hasClass('next'))
            p++;
        if (p < 0) {
            p = 0;
        }
        if (p > shortcodeone.find('.items_container .item').length - 1) {
            p = shortcodeone.find('.items_container .item').length - 1;
        }

        if (p == 0) {
            p = 0;
            shortcodeone.find('.navigation .prev').addClass('off');
        }
        if (p == pMax) {
            shortcodeone.find('.navigation .next').addClass('off');
        }

        shortcodeone.find('.items_container').attr('data-p', p)
        $(window).resize()

        if ($(window).width() < 767)
            $('html, body').animate({
                scrollTop: shortcodeone.offset().top - 100
            }, 500);
    })


    // Meetings
    // ===========================================================================

    jQuery('.shortcode_meetings').each(function() {
        var shortcode = $(this);
        var shortcode_arrow = '.' + $(this).attr('class').replace(/ /g, '.') + ' .arrow';
        var shortcode_year = '.' + $(this).attr('class').replace(/ /g, '.') + ' .nav_container .nav .scroll a';

        function shortcode_scrollMeetings() {
            wContainer = shortcode.find('.nav').width();
            if (wContainer <= 200)
                c = 2;
            if (wContainer > 200)
                c = 3;
            if (wContainer > 300)
                c = 4;
            if (wContainer > 400)
                c = 5;

            // Nav size
            wA = Math.ceil(shortcode.find('.nav').width() / c)
            shortcode.find('.nav_container .nav .scroll a').outerWidth(wA);

            // Nav move
            pos = shortcode.find('.nav_container .nav .scroll').attr('data-pos')
            shortcode.find('.nav_container .nav .scroll').css({
                'left': -(pos * wA + pos * 2) + 'px'
            })

            // Meet height
            hMax = 0;
            shortcode.find('.meetings_container .meet_container').each(function() {
                if (jQuery(this).height() > hMax)
                    hMax = jQuery(this).height();
            })
            shortcode.find('.meetings_container').height(hMax);

        }
        shortcode_scrollMeetings();

        $(document).on(clickHandler, shortcode_arrow, function(event) {
            pos = shortcode.find('.nav_container .nav .scroll').attr('data-pos')
            if ($(this).hasClass('prev'))
                pos--;

            if (pos < 0)
                pos = 0;
            if (pos > (shortcode.find('.nav_container .nav .scroll a').length) - 1 - c)
                pos = shortcode.find('.nav_container .nav .scroll a').length - 1 - c

            if ($(this).hasClass('next'))
                pos++;

            shortcode.find('.nav_container .nav .scroll').attr('data-pos', pos);
            shortcode_scrollMeetings();
        })

        $(document).on(clickHandler, shortcode_year, function(event) {
            year = $(this).data('year');

            shortcode.find('.nav_container .nav .scroll a').removeClass('active');
            shortcode.find('.nav_container .nav .scroll a[data-year="' + year + '"]').addClass('active');

            shortcode.find('.meetings_container .meet_container').removeClass('active');
            shortcode.find('.meetings_container .meet_container[data-year="' + year + '"]').addClass('active');
        })

        $(window).resize(function() {
            shortcode_scrollMeetings();
        });
    })


    // Feedcategory
    // ===========================================================================

    jQuery('.shortcode_feedcategory').each(function() {
        var shortcode = $(this);
        var shortcode_select = '.' + $(this).attr('class').replace(/ /g, '.') + ' select';

        function refreshFeed() {
            shortcode.find('.posts').addClass('load');

            $.ajax({
                type: "GET",
                url: domainTemplate + "/xhr/feedcategory.php",
                data: {
                    name: shortcode.find('select[name="category"] option:selected').data('name'),
                    category: shortcode.find('select[name="category"] option:selected').val(),
                    year: shortcode.find('select[name="year"] option:selected').val()
                }
            }).done(function(html) {
								shortcode.find('.shortcode_header').html(shortcode.find('select[name="category"] option:selected').data('name'));
                shortcode.find('.posts .items').html(html);
                shortcode.find('.items_container').attr('data-p', 0);
                shortcode.find('.items_container .items').css({
                    'top': 0
                });
                shortcode.find('.posts').removeClass('load');
                $(window).resize()
            });
        }

        $(document).on('change', shortcode_select, function(event) {
            refreshFeed();
        })
    })

    // Feedcategoryall
    // ===========================================================================

    jQuery('.shortcode_feedcategoryall').each(function() {
        var shortcode = $(this);
        var shortcode_select = '.' + $(this).attr('class').replace(/ /g, '.') + ' select';
        var shortcode_news = '.' + $(this).attr('class').replace(/ /g, '.') + ' .filter a.title';

        function refreshFeed() {
            shortcode.find('.posts').removeClass('hide');
            shortcode.find('.contents').addClass('hide');

            headerActive = shortcode.find('.filter.active select[name="category"] option:selected').data('name');
            catActive = shortcode.find('.filter.active select[name="category"] option:selected').val();
            yearActive = shortcode.find('.filter.active select[name="year"] option:selected').val()
            if (typeof(catActive) == 'undefined') {
                catActive = 'news';
            }

            if (typeof(yearActive) == 'undefined') {
                yearActive = '2018';
            }

            shortcode.find('.posts').addClass('load');

            $.ajax({
                type: "GET",
                url: domainTemplate + "/xhr/feedcategoryall.php",
                data: {
                    header: headerActive,
                    category: catActive,
                    year: yearActive
                }
            }).done(function(html) {
                shortcode.find('.posts .items').html(html);
                shortcode.find('.items_container').attr('data-p', 0);
                shortcode.find('.items_container .items').css({
                    'top': 0
                });
                shortcode.find('.posts').removeClass('load');
                $(window).resize()
            });
        }

        jQuery(document).on('change', shortcode_select, function(event) {
            jQuery('.shortcode_feedcategoryall .filter').removeClass('active');
            jQuery(this).closest('.filter').addClass('active');
            refreshFeed();
        })

        jQuery(document).on('click', shortcode_news, function(event) {
            jQuery('.shortcode_feedcategoryall .filter').removeClass('active');
            jQuery(this).closest('.filter').addClass('active');
            refreshFeed();
        })

        if (window.location.search.length > 0) {
            $('html, body').animate({
                scrollTop: shortcode.offset().top - $('#header .less').height() - 50
            }, 1000);
        }
    })


    // People
    // ===========================================================================

    jQuery('.shortcode_people').each(function() {
        var shortcode = $(this);

        function refreshPeople() {
            wList = shortcode.find('.list').width();

            shortcode.attr('data-size', 2);
            if (wList < 600)
                shortcode.attr('data-size', 1);
            if (wList > 900)
                shortcode.attr('data-size', 3);
        }
        refreshPeople();

        $(window).resize(function() {
            refreshPeople();
        });
    })




    $(document).on(clickHandler, '.backtotop', function(event) {
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
    })






    function scrollEffect() {
        if ($('.widget-top').length == 1) {
            posBody = $('body').scrollTop();
            posHtml = $('html').scrollTop();

            pos = 0;
            if (posBody > 0)
                pos = posBody
            if (posHtml > 0)
                pos = posHtml;

            pp = pos / ($('.widget-top .background').height() + $('.widget-top').position().top);
            $('.widget-top .background').css({
                'opacity': 1 - pp,
                'transform': 'translateY(' + (pp * 100 / 2) + '%)'
            })

            if (posHtml > 100 || posBody > 100)
                $('body').addClass('scrolled')
            else
                $('body').removeClass('scrolled')
        }
    }
    scrollEffect();



    $(window).scroll(function(event) {
        scrollEffect();
    });



    /* animated burger icon mobile*/
    jQuery(".navbar-toggle").on("click", function() {
        jQuery(this).toggleClass("active");
    });

    animationCSS();
    smoothscroll();

    // Bootstrap - on hover show submenu
    toggleNavbarMethod();
    /* Stara funkcja
    jQuery('.dropdown').hover(function(){
      jQuery('.dropdown-toggle', this).trigger('click');
    });
     // ADD SLIDEDOWN ANIMATION TO DROPDOWN //
    $('.dropdown').on('show.bs.dropdown', function(e){
      $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
    });

    // ADD SLIDEUP ANIMATION TO DROPDOWN //
    $('.dropdown').on('hide.bs.dropdown', function(e){
      $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
    });*/

    // scroll to colapsed element
    jQuery(".panel-default").on('shown.bs.collapse', function() {
        jQuery('html,body').animate({
                scrollTop: $(this).children('.panel-collapse').offset().top - 200
            },
            'slow');
    });

    //Menu mobile hover
    if ($(window).width() < 992) { // ograniczenie dla mobile menu
        //when mouse is on parent
        jQuery(".menu-item-has-children a").hover(function() {
            jQuery(".dropdown-menu").find("a").css("color", "white");
        });
        //when mouse is out
        jQuery(".menu-item-has-children").mouseout(function() {
            jQuery(".dropdown-menu").find("a").css("color", "#606060");
        });
    }
});

jQuery(window).load(function() {
    $('#loader').removeClass('show');
    setTimeout(function() {
        $('#loader').remove();
    }, 1500);



    // Feedcategoryall
    // ===========================================================================

    // jQuery('.shortcode_feedcategoryall').each(function() {
    //     var shortcode = $(this);
    //
    //     //setTimeout(function() {
    //         if (window.location.search.length > 0) {
    //             console.log('xxx', shortcode.offset().top - $('#header .less').height());
    //             $('html, body').animate({
    //                 scrollTop: shortcode.offset().top - $('#header .less').height() - 50
    //             }, 1000);
    //         }
    //     //}, 3000);
    //
    // })





    height_100();
    jQuery(window).trigger('resize');

    /* slick slider page */
    jQuery('.slick-slider-page').slick({
        adaptiveHeight: false,
        autoplay: true,
        autoplaySpeed: 10000,
        lazyLoad: 'progressive',
        slidesToShow: 1,
        infinite: true,
        dots: false,
        arrows: false,
        prevArrow: $('.slick-page-nav-wrapper .prev'),
        nextArrow: $('.slick-page-nav-wrapper .next')
    });
    jQuery('.slick-page-nav-wrapper .prev').click(function() {
        jQuery('.slick-slider').slick('slickPrev');
    })

    jQuery('.slick-page-nav-wrapper .next').click(function() {
        jQuery('.slick-slider').slick('slickNext');
    })
});


function toggleNavbarMethod() {
    jQuery(function($) {
        if ($(window).width() > 769) { // ograniczenie dla mobile menu
            $('.dropdown').hover(function() {
                $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

            }, function() {
                $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();
            });
            $('.dropdown > a').click(function() {
                //location.href = this.href;
            });
        }
    });
}

function height_100() {
    var screenheight = jQuery(window).height();
    var docheight = jQuery(document).height();
    jQuery('.height_100').css("min-height", screenheight + "px");
}

function roweqheight() {
    jQuery('.row-eq-height').each(function() {
        jQuery(this).children("[class*=' col']").height('auto');
        if ($(window).width() > 992) {
            var seth = 0;
            jQuery(this).children("[class*=' col']").each(function() {
                newh = jQuery(this).height();
                if (newh > seth) {
                    seth = newh;
                }
            });
            jQuery(this).children("[class*=' col']").height(seth);
        } else {
            jQuery(this).children("[class*=' col']").height('auto');
        }
    });
}
//PLUGINS


//WAYPOINT ANIMATE CSS
function is_touch_device() {
    return 'ontouchstart' in window // works on most browsers
        ||
        'onmsgesturechange' in window; // works on ie10
}

function animationCSS() {

    if (!is_touch_device()) {
        jQuery('*[data-animationcss]').addClass(" animated ");

        /* ================ ANIMATED CONTENT ================ */
        if (jQuery(".animated")[0]) {}

        jQuery('*[data-animationcss]').waypoint(function() {
            var animation = jQuery(this).attr('data-animationcss');
            jQuery(this).addClass(" animated anim_end " + animation);
        }, {
            offset: '90%',
            triggerOnce: true
        });
    }
}

/* full screen width element */
function fullWidthElement() {
    var wrapper = jQuery('body');
    jQuery('.full_width_element').each(function() {
        jQuery(this).css({
            "margin-left": 0,
            "margin-right": 0,
            "padding-left": 0,
            "padding-right": 0
        })
        var left = jQuery(this).offset().left;
        var right = jQuery(this).offset().right;
        jQuery(this).css({
            "margin-left": -left,
            "margin-right": -left
        })
        if (jQuery('.slick-slider-page').length) {
            jQuery('.slick-slider-page').slick('reinit');
        }
    });

}

function tabsheight() {
    var maxHeight = 0;
    $(".tab-content").height('auto');
    $('.tab-content .tab-pane.active').data('tabactiv', 1);
    $(".tab-content .tab-pane").each(function() {
        $(this).addClass("active");
        var height = $(this).height();
        maxHeight = height > maxHeight ? height : maxHeight;
        $(this).removeClass("active");
    });

    if ($(".tab-content .tab-pane.in").length) {
        $(".tab-content .tab-pane.in").addClass("active").data('tabactiv', '0');
    } else {
        $(".tab-content .tab-pane:first").addClass("active");
    }
    $(".tab-content").height(maxHeight);
}



/**
 * smootscroll special
 * use smoothscroll() in doc ready
 * https://wordpress.org/plugins/mousewheel-smooth-scroll/#developers
 */

function smoothscroll() {
    var D, M, T, E, C, H = {
        frameRate: 150,
        animationTime: 500,
        stepSize: 130,
        pulseAlgorithm: !0,
        pulseScale: 4,
        pulseNormalize: 1,
        accelerationDelta: 50,
        accelerationMax: 3,
        keyboardSupport: !0,
        arrowScroll: 1,
        fixedBackground: !0,
        excluded: ""
    }

    function e() {
        z.keyboardSupport && m("keydown", a)
    }

    function t() {
        if (!Y && document.body) {
            Y = !0;
            var t = document.body,
                o = document.documentElement,
                n = window.innerHeight,
                r = t.scrollHeight;
            if (A = document.compatMode.indexOf("CSS") >= 0 ? o : t, D = t, e(), top != self) O = !0;
            else if (te && r > n && (t.offsetHeight <= n || o.offsetHeight <= n)) {
                var a, i = document.createElement("div");
                i.style.cssText = "position:absolute; z-index:-10000; top:0; left:0; right:0; height:" + A.scrollHeight + "px", document.body.appendChild(i), T = function() {
                    a || (a = setTimeout(function() {
                        L || (i.style.height = "0", i.style.height = A.scrollHeight + "px", a = null)
                    }, 500))
                }, setTimeout(T, 10), m("resize", T);
                if (M = new W(T), M.observe(t, {
                        attributes: !0,
                        childList: !0,
                        characterData: !1
                    }), A.offsetHeight <= n) {
                    var l = document.createElement("div");
                    l.style.clear = "both", t.appendChild(l)
                }
            }
            z.fixedBackground || L || (t.style.backgroundAttachment = "scroll", o.style.backgroundAttachment = "scroll")
        }
    }

    function o() {
        M && M.disconnect(), w(I, r), w("mousedown", i), w("keydown", a), w("resize", T), w("load", t)
    }

    function n(e, t, o) {
        if (p(t, o), 1 != z.accelerationMax) {
            var n = Date.now() - q;
            if (n < z.accelerationDelta) {
                var r = (1 + 50 / n) / 2;
                r > 1 && (r = Math.min(r, z.accelerationMax), t *= r, o *= r)
            }
            q = Date.now()
        }
        if (R.push({
                x: t,
                y: o,
                lastX: t < 0 ? .99 : -.99,
                lastY: o < 0 ? .99 : -.99,
                start: Date.now()
            }), !j) {
            var a = e === document.body,
                i = function(n) {
                    for (var r = Date.now(), l = 0, c = 0, u = 0; u < R.length; u++) {
                        var d = R[u],
                            s = r - d.start,
                            f = s >= z.animationTime,
                            m = f ? 1 : s / z.animationTime;
                        z.pulseAlgorithm && (m = x(m));
                        var h = d.x * m - d.lastX >> 0,
                            w = d.y * m - d.lastY >> 0;
                        l += h, c += w, d.lastX += h, d.lastY += w, f && (R.splice(u, 1), u--)
                    }
                    a ? window.scrollBy(l, c) : (l && (e.scrollLeft += l), c && (e.scrollTop += c)), t || o || (R = []), R.length ? _(i, e, 1e3 / z.frameRate + 1) : j = !1
                };
            _(i, e, 0), j = !0
        }
    }

    function r(e) {
        Y || t();
        var o = e.target;
        if (e.defaultPrevented || e.ctrlKey) return !0;
        if (h(D, "embed") || h(o, "embed") && /\.pdf/i.test(o.src) || h(D, "object") || o.shadowRoot) return !0;
        var r = -e.wheelDeltaX || e.deltaX || 0,
            a = -e.wheelDeltaY || e.deltaY || 0;
        N && (e.wheelDeltaX && y(e.wheelDeltaX, 120) && (r = e.wheelDeltaX / Math.abs(e.wheelDeltaX) * -120), e.wheelDeltaY && y(e.wheelDeltaY, 120) && (a = e.wheelDeltaY / Math.abs(e.wheelDeltaY) * -120)), r || a || (a = -e.wheelDelta || 0), 1 === e.deltaMode && (r *= 40, a *= 40);
        var i = u(o);
        return i ? !!v(a) || (Math.abs(r) > 1.2 && (r *= z.stepSize / 120), Math.abs(a) > 1.2 && (a *= z.stepSize / 120), n(i, r, a), e.preventDefault(), void l()) : !O || !J || (Object.defineProperty(e, "target", {
            value: window.frameElement
        }), parent.wheel(e))
    }

    function a(e) {
        var t = e.target,
            o = e.ctrlKey || e.altKey || e.metaKey || e.shiftKey && e.keyCode !== K.spacebar;
        document.body.contains(D) || (D = document.activeElement);
        var r = /^(button|submit|radio|checkbox|file|color|image)$/i;
        if (e.defaultPrevented || /^(textarea|select|embed|object)$/i.test(t.nodeName) || h(t, "input") && !r.test(t.type) || h(D, "video") || g(e) || t.isContentEditable || o) return !0;
        if ((h(t, "button") || h(t, "input") && r.test(t.type)) && e.keyCode === K.spacebar) return !0;
        if (h(t, "input") && "radio" == t.type && P[e.keyCode]) return !0;
        var a = 0,
            i = 0,
            c = u(D);
        if (!c) return !O || !J || parent.keydown(e);
        var d = c.clientHeight;
        switch (c == document.body && (d = window.innerHeight), e.keyCode) {
            case K.up:
                i = -z.arrowScroll;
                break;
            case K.down:
                i = z.arrowScroll;
                break;
            case K.spacebar:
                i = -(e.shiftKey ? 1 : -1) * d * .9;
                break;
            case K.pageup:
                i = .9 * -d;
                break;
            case K.pagedown:
                i = .9 * d;
                break;
            case K.home:
                i = -c.scrollTop;
                break;
            case K.end:
                var s = c.scrollHeight - c.scrollTop - d;
                i = s > 0 ? s + 10 : 0;
                break;
            case K.left:
                a = -z.arrowScroll;
                break;
            case K.right:
                a = z.arrowScroll;
                break;
            default:
                return !0
        }
        n(c, a, i), e.preventDefault(), l()
    }

    function i(e) {
        D = e.target
    }

    function l() {
        clearTimeout(E), E = setInterval(function() {
            F = {}
        }, 1e3)
    }

    function c(e, t) {
        for (var o = e.length; o--;) F[V(e[o])] = t;
        return t
    }

    function u(e) {
        var t = [],
            o = document.body,
            n = A.scrollHeight;
        do {
            var r = F[V(e)];
            if (r) return c(t, r);
            if (t.push(e), n === e.scrollHeight) {
                var a = s(A) && s(o) || f(A);
                if (O && d(A) || !O && a) return c(t, $())
            } else if (d(e) && f(e)) return c(t, e)
        } while (e = e.parentElement)
    }

    function d(e) {
        return e.clientHeight + 10 < e.scrollHeight
    }

    function s(e) {
        return "hidden" !== getComputedStyle(e, "").getPropertyValue("overflow-y")
    }

    function f(e) {
        var t = getComputedStyle(e, "").getPropertyValue("overflow-y");
        return "scroll" === t || "auto" === t
    }

    function m(e, t) {
        window.addEventListener(e, t, !1)
    }

    function w(e, t) {
        window.removeEventListener(e, t, !1)
    }

    function h(e, t) {
        return (e.nodeName || "").toLowerCase() === t.toLowerCase()
    }

    function p(e, t) {
        e = e > 0 ? 1 : -1, t = t > 0 ? 1 : -1, X.x === e && X.y === t || (X.x = e, X.y = t, R = [], q = 0)
    }

    function v(e) {
        if (e) return B.length || (B = [e, e, e]), e = Math.abs(e), B.push(e), B.shift(), clearTimeout(C), C = setTimeout(function() {
            try {
                localStorage.SS_deltaBuffer = B.join(",")
            } catch (e) {}
        }, 1e3), !b(120) && !b(100)
    }

    function y(e, t) {
        return Math.floor(e / t) == e / t
    }

    function b(e) {
        return y(B[0], e) && y(B[1], e) && y(B[2], e)
    }

    function g(e) {
        var t = e.target,
            o = !1;
        if (-1 != document.URL.indexOf("www.youtube.com/watch"))
            do {
                if (o = t.classList && t.classList.contains("html5-video-controls")) break
            } while (t = t.parentNode);
        return o
    }

    function S(e) {
        var t, o;
        return (e *= z.pulseScale) < 1 ? t = e - (1 - Math.exp(-e)) : (e -= 1, t = (o = Math.exp(-1)) + (1 - Math.exp(-e)) * (1 - o)), t * z.pulseNormalize
    }

    function x(e) {
        return e >= 1 ? 1 : e <= 0 ? 0 : (1 == z.pulseNormalize && (z.pulseNormalize /= S(1)), S(e))
    }

    function k(e) {
        for (var t in e) H.hasOwnProperty(t) && (z[t] = e[t])
    }
    var z = H,
        L = !1,
        O = !1,
        X = {
            x: 0,
            y: 0
        },
        Y = !1,
        A = document.documentElement,
        B = [],
        N = /^Mac/.test(navigator.platform),
        K = {
            left: 37,
            up: 38,
            right: 39,
            down: 40,
            spacebar: 32,
            pageup: 33,
            pagedown: 34,
            end: 35,
            home: 36
        },
        P = {
            37: 1,
            38: 1,
            39: 1,
            40: 1
        },
        R = [],
        j = !1,
        q = Date.now(),
        V = function() {
            var e = 0;
            return function(t) {
                return t.uniqueID || (t.uniqueID = e++)
            }
        }(),
        F = {};
    if (window.localStorage && localStorage.SS_deltaBuffer) try {
        B = localStorage.SS_deltaBuffer.split(",")
    } catch (e) {}
    var I, _ = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function(e, t, o) {
            window.setTimeout(e, o || 1e3 / 60)
        },
        W = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver,
        $ = function() {
            var e;
            return function() {
                if (!e) {
                    var t = document.createElement("div");
                    t.style.cssText = "height:10000px;width:1px;", document.body.appendChild(t);
                    var o = document.body.scrollTop;
                    document.documentElement.scrollTop, window.scrollBy(0, 3), e = document.body.scrollTop != o ? document.body : document.documentElement, window.scrollBy(0, -3), document.body.removeChild(t)
                }
                return e
            }
        }(),
        U = window.navigator.userAgent,
        G = /Edge/.test(U),
        J = /chrome/i.test(U) && !G,
        Q = /safari/i.test(U) && !G,
        Z = /mobile/i.test(U),
        ee = /Windows NT 6.1/i.test(U) && /rv:11/i.test(U),
        te = Q && (/Version\/8/i.test(U) || /Version\/9/i.test(U)),
        oe = (J || Q || ee) && !Z;
    "onwheel" in document.createElement("div") ? I = "wheel" : "onmousewheel" in document.createElement("div") && (I = "mousewheel"), I && oe && (m(I, r), m("mousedown", i), m("load", t)), k.destroy = o, window.SmoothScrollOptions && k(window.SmoothScrollOptions), "function" == typeof define && define.amd ? define(function() {
        return k
    }) : "object" == typeof exports ? module.exports = k : window.SmoothScroll = k;
}
