jQuery(document).ready(function($) {

    // Perform AJAX login on form submit
    $('form#login p.status').slideUp().removeClass("alert alert-danger")
    $('form#login').on('submit', function(e){
        $('form#login p.status').slideDown().removeClass("alert alert-danger").text(ajax_login_object.loadingmessage);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_login_object.ajaxurl,
            data: {
                'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
                'username': $('form#login #username').val(),
                'password': $('form#login #password').val(),
                'security': $('form#login #security').val() },
            success: function(data){
                $('form#login p.status').removeClass("alert alert-danger").addClass('alert alert-success').text(data.message);
                if (data.loggedin == true){
                    document.location.href = ajax_login_object.redirecturl;
                }
            },
            error: function(data){
                $('form#login p.status').addClass("alert alert-danger").text('Login lub hasło są nieprawidłowe');
            	console.log(data);
            }
        });
        e.preventDefault();
    });

});
