<?php
/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.5.2
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */

add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );
/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function my_theme_register_required_plugins() {
  /*
   * Array of plugin arrays. Required keys are name and slug.
   * If the source is NOT from the .org repo, then source is also required.
   */

  $plugins = array(

    array(
      'name'               => 'Page Builder by SiteOrigin', // The plugin name.
      'slug'               => 'siteorigin-panels', // The plugin slug (typically the folder name).
      //'source'             => get_stylesheet_directory() . '/lib/plugins/tgm-example-plugin.zip', // The plugin source local.
      //'source'             => 'https://downloads.wordpress.org/plugin/black-studio-tinymce-widget.2.2.8.zip', // The plugin source external.
      'required'           => true, // If false, the plugin is only 'recommended' instead of required.
      'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
      'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
      'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
      'external_url'       => '', // If set, overrides default API URL and points to an external URL.
      'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
    ),
    array(
      'name'               => 'SO Widgets Bundle', // The plugin name.
      'slug'               => 'so-widgets-bundle', // The plugin slug (typically the folder name).
      //'source'             => get_stylesheet_directory() . '/lib/plugins/tgm-example-plugin.zip', // The plugin source local.
      //'source'             => 'https://downloads.wordpress.org/plugin/black-studio-tinymce-widget.2.2.8.zip', // The plugin source external.
      'required'           => true, // If false, the plugin is only 'recommended' instead of required.
      'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
      'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
      'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
      'external_url'       => '', // If set, overrides default API URL and points to an external URL.
      'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
    ),

    array(
      'name'               => 'Ajax Thumbnail Rebuild', // The plugin name.
      'slug'               => 'ajax-thumbnail-rebuild', // The plugin slug (typically the folder name).
      //'source'             => get_stylesheet_directory() . '/lib/plugins/tgm-example-plugin.zip', // The plugin source local.
      //'source'             => 'https://downloads.wordpress.org/plugin/black-studio-tinymce-widget.2.2.8.zip', // The plugin source external.
      'required'           => false, // If false, the plugin is only 'recommended' instead of required.
      'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
      'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
      'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
      'external_url'       => '', // If set, overrides default API URL and points to an external URL.
      'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
    ),
    array(
      'name'               => 'All In One SEO Pack', // The plugin name.
      'slug'               => 'all-in-one-seo-pack', // The plugin slug (typically the folder name).
      //'source'             => get_stylesheet_directory() . '/lib/plugins/tgm-example-plugin.zip', // The plugin source local.
      //'source'             => 'https://downloads.wordpress.org/plugin/black-studio-tinymce-widget.2.2.8.zip', // The plugin source external.
      'required'           => false, // If false, the plugin is only 'recommended' instead of required.
      'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
      'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
      'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
      'external_url'       => '', // If set, overrides default API URL and points to an external URL.
      'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
    ),
    array(
      'name'               => 'All In One WP Security and Firewall', // The plugin name.
      'slug'               => 'all-in-one-wp-security-and-firewall', // The plugin slug (typically the folder name).
      //'source'             => get_stylesheet_directory() . '/lib/plugins/tgm-example-plugin.zip', // The plugin source local.
      //'source'             => 'https://downloads.wordpress.org/plugin/black-studio-tinymce-widget.2.2.8.zip', // The plugin source external.
      'required'           => false, // If false, the plugin is only 'recommended' instead of required.
      'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
      'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
      'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
      'external_url'       => '', // If set, overrides default API URL and points to an external URL.
      'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
    ),
    array(
      'name'               => 'Wp Super Cache', // The plugin name.
      'slug'               => 'wp-super-cache', // The plugin slug (typically the folder name).
      //'source'             => get_stylesheet_directory() . '/lib/plugins/tgm-example-plugin.zip', // The plugin source local.
      //'source'             => 'https://downloads.wordpress.org/plugin/black-studio-tinymce-widget.2.2.8.zip', // The plugin source external.
      'required'           => false, // If false, the plugin is only 'recommended' instead of required.
      'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
      'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
      'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
      'external_url'       => '', // If set, overrides default API URL and points to an external URL.
      'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
    ),
    array(
      'name'               => 'Better Delete Revision', // The plugin name.
      'slug'               => 'better-delete-revision', // The plugin slug (typically the folder name).
      //'source'             => get_stylesheet_directory() . '/lib/plugins/tgm-example-plugin.zip', // The plugin source local.
      //'source'             => 'https://downloads.wordpress.org/plugin/black-studio-tinymce-widget.2.2.8.zip', // The plugin source external.
      'required'           => false, // If false, the plugin is only 'recommended' instead of required.
      'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
      'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
      'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
      'external_url'       => '', // If set, overrides default API URL and points to an external URL.
      'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
    ),
   array(
      'name'               => 'Simple Page Ordering', // The plugin name.
      'slug'               => 'simple-page-ordering', // The plugin slug (typically the folder name).
      //'source'             => get_stylesheet_directory() . '/lib/plugins/tgm-example-plugin.zip', // The plugin source local.
      //'source'             => 'https://downloads.wordpress.org/plugin/black-studio-tinymce-widget.2.2.8.zip', // The plugin source external.
      'required'           => false, // If false, the plugin is only 'recommended' instead of required.
      'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
      'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
      'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
      'external_url'       => '', // If set, overrides default API URL and points to an external URL.
      'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
    ),
   array(
      'name'               => 'Intuitive Custom Post Order', // The plugin name.
      'slug'               => 'intuitive-custom-post-order', // The plugin slug (typically the folder name).
      //'source'             => get_stylesheet_directory() . '/lib/plugins/tgm-example-plugin.zip', // The plugin source local.
      //'source'             => 'https://downloads.wordpress.org/plugin/black-studio-tinymce-widget.2.2.8.zip', // The plugin source external.
      'required'           => false, // If false, the plugin is only 'recommended' instead of required.
      'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
      'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
      'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
      'external_url'       => '', // If set, overrides default API URL and points to an external URL.
      'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
    ),
    array(
      'name'               => 'Duplicate Post', // The plugin name.
      'slug'               => 'duplicate-post', // The plugin slug (typically the folder name).
      //'source'             => get_stylesheet_directory() . '/lib/plugins/tgm-example-plugin.zip', // The plugin source local.
      //'source'             => 'https://downloads.wordpress.org/plugin/black-studio-tinymce-widget.2.2.8.zip', // The plugin source external.
      'required'           => false, // If false, the plugin is only 'recommended' instead of required.
      'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
      'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
      'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
      'external_url'       => '', // If set, overrides default API URL and points to an external URL.
      'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
    ),


/*    // This is an example of how to include a plugin from an arbitrary external source in your theme.
    array(
      'name'         => 'TGM New Media Plugin', // The plugin name.
      'slug'         => 'tgm-new-media-plugin', // The plugin slug (typically the folder name).
      'source'       => 'https://s3.amazonaws.com/tgm/tgm-new-media-plugin.zip', // The plugin source.
      'required'     => true, // If false, the plugin is only 'recommended' instead of required.
      'external_url' => 'https://github.com/thomasgriffin/New-Media-Image-Uploader', // If set, overrides default API URL and points to an external URL.
    ),

    // This is an example of how to include a plugin from a GitHub repository in your theme.
    // This presumes that the plugin code is based in the root of the GitHub repository
    // and not in a subdirectory ('/src') of the repository.
    array(
      'name'      => 'Adminbar Link Comments to Pending',
      'slug'      => 'adminbar-link-comments-to-pending',
      'source'    => 'https://github.com/jrfnl/WP-adminbar-comments-to-pending/archive/master.zip',
    ),

    // This is an example of how to include a plugin from the WordPress Plugin Repository.
    array(
      'name'      => 'BuddyPress',
      'slug'      => 'buddypress',
      'required'  => false,
    ),

    // This is an example of the use of 'is_callable' functionality. A user could - for instance -
    // have WPSEO installed *or* WPSEO Premium. The slug would in that last case be different, i.e.
    // 'wordpress-seo-premium'.
    // By setting 'is_callable' to either a function from that plugin or a class method
    // `array( 'class', 'method' )` similar to how you hook in to actions and filters, TGMPA can still
    // recognize the plugin as being installed.
    array(
      'name'        => 'WordPress SEO by Yoast',
      'slug'        => 'wordpress-seo',
      'is_callable' => 'wpseo_init',
    ),*/

  );

  /*
   * Array of configuration settings. Amend each line as needed.
   *
   * TGMPA will start providing localized text strings soon. If you already have translations of our standard
   * strings available, please help us make TGMPA even better by giving us access to these translations or by
   * sending in a pull-request with .po file(s) with the translations.
   *
   * Only uncomment the strings in the config array if you want to customize the strings.
   */
  $tgmpaconfig = array(
    'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
    'default_path' => '',                      // Default absolute path to bundled plugins.
    'menu'         => 'tgmpa-install-plugins', // Menu slug.
    'parent_slug'  => 'themes.php',            // Parent menu slug.
    'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
    'has_notices'  => true,                    // Show admin notices or not.
    'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
    'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
    'is_automatic' => true,                   // Automatically activate plugins after installation or not.
    'message'      => '',                      // Message to output right before the plugins table.


    'strings'      => array(
        'page_title'                      => __( 'Install Required Plugins', 'engine' ),
        'menu_title'                      => __( 'Install Plugins', 'engine' ),
        'installing'                      => __( 'Installing Plugin: %s', 'engine' ),
        'updating'                        => __( 'Updating Plugin: %s', 'engine' ),
        'oops'                            => __( 'Something went wrong with the plugin API.', 'engine' ),
        'notice_can_install_required'     => _n_noop(
            'This theme requires the following plugin: %1$s.',
            'This theme requires the following plugins: %1$s.',
            'engine'
        ),
        'notice_can_install_recommended'  => _n_noop(
            'This theme recommends the following plugin: %1$s.',
            'This theme recommends the following plugins: %1$s.',
            'engine'
        ),
        'notice_ask_to_update'            => _n_noop(
            'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
            'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
            'engine'
        ),
        'notice_ask_to_update_maybe'      => _n_noop(
            'There is an update available for: %1$s.',
            'There are updates available for the following plugins: %1$s.',
            'engine'
        ),
        'notice_can_activate_required'    => _n_noop(
            'The following required plugin is currently inactive: %1$s.',
            'The following required plugins are currently inactive: %1$s.',
            'engine'
        ),
        'notice_can_activate_recommended' => _n_noop(
            'The following recommended plugin is currently inactive: %1$s.',
            'The following recommended plugins are currently inactive: %1$s.',
            'engine'
        ),
        'install_link'                    => _n_noop(
            'Begin installing plugin',
            'Begin installing plugins',
            'engine'
        ),
        'update_link' 					  => _n_noop(
            'Begin updating plugin',
            'Begin updating plugins',
            'engine'
        ),
        'activate_link'                   => _n_noop(
            'Begin activating plugin',
            'Begin activating plugins',
            'engine'
        ),
        'return'                          => __( 'Return to Required Plugins Installer', 'engine' ),
        'plugin_activated'                => __( 'Plugin activated successfully.', 'engine' ),
        'activated_successfully'          => __( 'The following plugin was activated successfully:', 'engine' ),
        'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'engine' ),
        'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'engine' ),
        'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'engine' ),
        'dismiss'                         => __( 'Dismiss this notice', 'engine' ),
        'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'engine' ),
        'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'engine' ),

        'nag_type'                        => 'updated', // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
    ),

  );

  tgmpa( $plugins, $tgmpaconfig );
}
