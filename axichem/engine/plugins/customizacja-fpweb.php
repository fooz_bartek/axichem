<?php
/*
Plugin Name: Customizacja Systemu
Plugin URI:
Description: Podmienia elementy systemu WP na elementy identyfikacyjne
Version: 1
Author: Wojciech Zdziejowski
Author URI:
License: GPL2

*/
// CUSTOM LOGIN CSS
function login_styles()
{
    echo('
<style type="text/css">
html {
    background: #fff !important;
}
body {
    color: #3F3F41 !important;
    font-family: tahoma,sans-serif !important;
    font-size: 13px !important;
    margin: 0px auto; !important;
    text-align: center !important;
}
.login h1{
    margin: 0px auto; !important;
    text-align: center !important;
    width: 100% !important;
    padding: 30px 10px !important;
    -webkit-transition: all 0.4s linear;
    -moz-transition: all 0.4s linear;
    -o-transition: all 0.4s linear;
    transition: all 0.4s linear;
}

.login h1 a{
    margin: 0px auto; !important;
    text-align: center !important;
    background: url(' . get_template_directory_uri() . '/img/author.png) no-repeat top center !important;
    width: 259px !important;
    height: 35px !important;
    text-indent: -9999px !important;
    overflow: hidden !important;
    padding-bottom: 15px !important;
    display: block !important;
    -webkit-transition: all 0.4s linear;
    -moz-transition: all 0.4s linear;
    -o-transition: all 0.4s linear;
    transition: all 0.4s linear;
}
#login {
    width: 460px;
    margin: 0px auto !important;
    padding: 0px !important;
}
#login form, .login .message {
    width: 400px !important;
    margin: 10px auto; !important;
    padding: 20px;
    overflow: hidden;
    }
.login #nav {
    margin: 24px 0 0 !important;
    padding: 0 50px !important;
    text-align: left !important;
    font-weight: 900 !important;
}
.login #backtoblog{
    margin: 10px auto !important;
    text-align: center !important;
}
.login #backtoblog a{
    -webkit-border-radius: 3px !important;
    border-radius: 3px !important;
    border-width: 1px !important;
    border-style: solid !important;
    border-color: #292922 !important;
    padding: 20px 28px !important;
    width: 300px !important;
    margin: 10px auto !important;
    font-weight: 900 !important;
    display: block !important;
    margin-top: 60px !important;
    -webkit-transition: all 0.4s linear;
    -moz-transition: all 0.4s linear;
    -o-transition: all 0.4s linear;
    transition: all 0.4s linear;
}
.login #backtoblog a:hover{
    background-color: #000;
    color: #fff;
    -webkit-transition: all 0.4s linear;
    -moz-transition: all 0.4s linear;
    -o-transition: all 0.4s linear;
    transition: all 0.4s linear;
}
.login #nav a, .login #backtoblog a {
    color: #292922!important;
    text-shadow: #292922 0px 0px 0px !important;
    -webkit-transition: all 0.4s linear;
    -moz-transition: all 0.4s linear;
    -o-transition: all 0.4s linear;
    transition: all 0.4s linear;
}
.login #nav a:hover{
    color: #292922!important;
    -webkit-transition: all 0.4s linear;
    -moz-transition: all 0.4s linear;
    -o-transition: all 0.4s linear;
    transition: all 0.4s linear;
}
.login #backtoblog a:hover {
    color: #fff !important;
    -webkit-transition: all 0.4s linear;
    -moz-transition: all 0.4s linear;
    -o-transition: all 0.4s linear;
    transition: all 0.4s linear;
}

.login form, #login_error, .login .message{
    -webkit-border-radius: 3px !important;
    border-radius: 3px !important;
    border-width: 1px !important;
    border-style: solid !important;
    border-color: #292922 !important;
    padding: 20px 28px;
    width: 300px;
    margin: 10px auto; !important;
}

input.button-primary, button.button-primary, a.button-primary {
    border-color: #000000 !important;
    font-weight: bold !important;
    color: white !important;
    background: #1F1F1F !important;
    text-shadow: rgba(0, 0, 0, 0.3) 0 -1px 0 !important;
    -webkit-transition: all 0.4s linear;
    -moz-transition: all 0.4s linear;
    -o-transition: all 0.4s linear;
    transition: all 0.4s linear;
}
input.button-primary:hover, button.button-primary:hover, a.button-primary:hover {
    background: #373737 !important;
    -webkit-transition: all 0.4s linear;
    -moz-transition: all 0.4s linear;
    -o-transition: all 0.4s linear;
    transition: all 0.4s linear;
}

.page_title{
	padding-top: 60px;
	font-size: 25px;
	text-align:center;
}
.page_title small{
	font-size: 14px;
}
.login form input[type=checkbox]{
    margin-top: -4px !important;
}

/* login popup */
#wp-auth-check-wrap #wp-auth-check{
    width: 470px !important;
}
#wp-auth-check-wrap #wp-auth-check .page_title {
    margin-top: 40px !important;
}
</style>
');
}

add_action('login_head', 'login_styles');

// CUSTOM ADMIN LOGIN LOGO LINK

function change_wp_login_url()
{
    //echo bloginfo('url');
}

add_filter('login_headerurl', 'change_wp_login_url');

// CUSTOM ADMIN LOGIN LOGO & ALT TEXT

function change_wp_login_title()
{
    echo '<div class="page_title"> ' . get_option('blogname') . ' <br><small> supported by:</small></div>';

}

add_filter('login_headertitle', 'change_wp_login_title');

// CUSTOM ADMIN DASHBOARD HEADER LOGO

/*function custom_admin_logo() {
    echo ('<style type="text/css">
	#header-logo { background-image: url('. plugins_url() .'/customizacja-fpweb/img/db_ico.png) !important; }
	#wp-admin-bar-wp-logo > .ab-item .ab-icon { background-image: url('. plugins_url() .'/customizacja-fpweb/img/db_ico.png) !important; background-position: 0px !important }
	</style>
	');
}
add_action('admin_head', 'custom_admin_logo');  */

// Admin footer modification + version wp in dashboard

function remove_footer_admin()
{
    echo '<span id="footer-thankyou"><a href="" target="_blank">Project and instlation mediablazegroup.com</a></span>';
    echo '<script type="text/javascript">';
    echo ';(function($){ $("#wp-version-message, .versions p").hide(); })(jQuery);';
    echo '</script>';

}

add_filter('admin_footer_text', 'remove_footer_admin');


/*function remove_admin_bar_links() {
	global $wp_admin_bar;
	if(!current_user_can('administrator')) {
	$wp_admin_bar->remove_menu('wp-logo');
	$wp_admin_bar->remove_menu('view-site');
	$wp_admin_bar->remove_menu('comments');
	}
}
add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );

*/


?>
