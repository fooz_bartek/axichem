<!-- content l-col -->
<section class="container" id="content">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-8 col-sm-push-6 col-md-push-4">
			<?php
					get_template_part('loop');
			?>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4 col-sm-pull-6 col-md-pull-8">
			<?php
				if ( is_active_sidebar( 'left-sidebar' ) ) {
					dynamic_sidebar( 'left-sidebar' );
				}
			?>
		</div>
	</div>
</section>
