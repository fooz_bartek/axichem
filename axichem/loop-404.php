
<div class="loop">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title">
				<h1>404</h1>
			</div>
		</div>
	</div>

	<div class="row bottom-50">
		<div class="col-xs-12">
			<h2>
		<?php echo _e( 'Nothing to Show Right Now', 'theme'); ?>
			</h2>
		</div>
	</div>

</div> <!-- /.row -->

<!-- Next and previous categories -->

</div>
