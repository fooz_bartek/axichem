<?php
/**
Version: 2
Lastmod: 01.08.2017
**/

/*=============================
 * GLOBALNIE
 *=============================
 */
define('TEMPL_NAME', 'axichem');
define('TEMPL_OWNER_URL', '');
define('TEMPL_AUTHOR', 'Axichem');

define('TEMPL_DIR', get_template_directory_uri() . '/');

require_once('engine/includer.php');
require_once('theme_inc/includer.php');

/*=============================
 * SKRYPTY I STYLE
 *=============================
 */
if (!is_admin()) {

    // style
    wp_register_style('slick', TEMPL_DIR . 'js/slick-1.6.0/slick/slick.css');
    wp_enqueue_style('slick');
    wp_register_style('slick-theme', TEMPL_DIR . 'js/slick-1.6.0/slick/slick-theme.css');
    wp_enqueue_style('slick-theme');
    wp_register_style('animatecss', TEMPL_DIR . 'js/waypoints-animate/animate.css');
    wp_enqueue_style('animatecss');
    wp_register_style('niceselect', TEMPL_DIR . 'css/nice-select.css');
    wp_enqueue_style('niceselect');
    wp_register_style('main-' . TEMPL_NAME, TEMPL_DIR . 'css/main.css');
    wp_enqueue_style('main-' . TEMPL_NAME);

    // script
    wp_enqueue_script('jquery');
    wp_enqueue_script('slick', TEMPL_DIR . 'js/slick-1.6.0/slick/slick.min.js', array('jquery'));
    wp_enqueue_script('waypoint', TEMPL_DIR . 'js/waypoints-animate/jquery.waypoints.min.js', array('jquery'));
    wp_enqueue_script('niceselect', TEMPL_DIR . 'js/jquery.nice-select.min.js', array('jquery'));
		wp_enqueue_script('countUp', TEMPL_DIR.'js/countUp.js', array('jquery'));
    wp_enqueue_script('validate', TEMPL_DIR.'js/jquery.validate.min.js', array('jquery'));
    wp_enqueue_script('validate_add', TEMPL_DIR.'js/additional-methods.js', array('jquery'));

    wp_enqueue_script('mainjs', TEMPL_DIR . 'js/main.js');

    if (is_singular() && get_option('thread_comments'))
        wp_enqueue_script('comment-reply');
}

wp_enqueue_script('tinymce-add2', TEMPL_DIR . 'js/tinymce-add.js');


/*
 * =====================
 *       NAVMENU
 * =====================
 */
register_nav_menu('menu', 'Główne menu');
if (!function_exists('fpweb_page_menu')) {
    function fpweb_page_menu($args = array())
    {
        //Add bootstrap classes
        $echo = $args['echo'];
        $args['echo'] = 0;
        $args['menu_class'] = $args['menu_class'] . ' main-menu navbar collapse navbar-collapse';
        $string = wp_page_menu($args);
        //ul
        $string = str_replace('<ul>', '<ul class="nav navbar-nav">', $string);
        //li dropdown
        $string = str_replace('page_item_has_children', 'dropdown page_item_has_children', $string);
        //li a dropdown-toggle
        $string = str_replace('page_item_has_children"><a ', 'page_item_has_children"><a class="dropdown-toggle" data-toggle="dropdown" ', $string);
        // koniec a w menu
        $string = str_replace('</a><ul', ' <b class="caret"></b></a><ul', $string);
        //ul children
        $string = str_replace("class='children", "class='children dropdown-menu", $string);

        if ($echo)
            echo $string;
        else
            return $string;

    }
} //end if

// Widgets menu
function wpb_custom_new_menu() {
	register_nav_menu('my-custom-menu',__( 'Widgets menu' ));
}
add_action( 'init', 'wpb_custom_new_menu' );
/*
 *=============================
 * SIDEBAR
 *=============================
 */


$argsocial = array(
     'name' => sprintf(__('Social', 'engine'), $i),
     'id' => "social-sidebar",
     'description' => '',
     'class' => 'sidebar-wrapper',
     'before_widget' => '<div id="%1$s" class="widget %2$s">',
     'after_widget' => "</div>\n",
     'before_title' => '<h4 class="widget-title">',
     'after_title' => "</h4>\n",
);

register_sidebar($argsocial);


$argshead = array(
    'name' => sprintf(__('Footer - Our Brands', 'engine'), $i),
    'id' => "ourbrands-sidebar",
    'description' => '',
    'class' => 'sidebar-sidebar-wrapper row',
    'before_widget' => '<div id="%1$s" class="widget-header  col-xs-12 %2$s">',
    'after_widget' => "</div>\n",
    'before_title' => '',
    'after_title' => "\n",
);
register_sidebar($argshead);


$argsfooter = array(
    'name' => sprintf(__('Footer sidebar', 'engine'), $i),
    'id' => "footer-sidebar",
    'description' => '',
    'class' => 'sidebar-sidebar-wrapper row',
    'before_widget' => '<div id="%1$s" class="widget-header  col-xs-12 %2$s">',
    'after_widget' => "</div>\n",
    'before_title' => '',
    'after_title' => "\n",
);
register_sidebar($argsfooter);

$argssingle = array(
    'name' => sprintf(__('Single post', 'engine'), $i),
    'id' => "single-sidebar",
    'description' => '',
    'class' => 'sidebar-sidebar-wrapper row',
    'before_widget' => '<div id="%1$s" class="widget-header  col-xs-12 %2$s">',
    'after_widget' => "</div>\n",
    'before_title' => '',
    'after_title' => "\n",
);
register_sidebar($argssingle);

$argscorporategovernance = array(
    'name' => sprintf(__('Corporate Governance', 'engine'), $i),
    'id' => "corporate-governance-sidebar",
    'description' => '',
    'class' => 'sidebar-sidebar-wrapper row',
    'before_widget' => '<div id="%1$s" class="widget-header  col-xs-12 %2$s">',
    'after_widget' => "</div>\n",
    'before_title' => '',
    'after_title' => "\n",
);
register_sidebar($argscorporategovernance);

$argspressroom = array(
    'name' => sprintf(__('Press room', 'engine'), $i),
    'id' => "press-room-sidebar",
    'description' => '',
    'class' => 'sidebar-sidebar-wrapper row',
    'before_widget' => '<div id="%1$s" class="widget-header  col-xs-12 %2$s">',
    'after_widget' => "</div>\n",
    'before_title' => '',
    'after_title' => "\n",
);
register_sidebar($argspressroom);

$argssearch = array(
    'name' => sprintf(__('Search', 'engine'), $i),
    'id' => "search-sidebar",
    'description' => '',
    'class' => 'sidebar-sidebar-wrapper row',
    'before_widget' => '<div id="%1$s" class="widget-header  col-xs-12 %2$s">',
    'after_widget' => "</div>\n",
    'before_title' => '',
    'after_title' => "\n",
);
register_sidebar($argssearch);

$args404 = array(
    'name' => sprintf(__('404', 'engine'), $i),
    'id' => "404-sidebar",
    'description' => '',
    'class' => 'sidebar-sidebar-wrapper row',
    'before_widget' => '<div id="%1$s" class="widget-header  col-xs-12 %2$s">',
    'after_widget' => "</div>\n",
    'before_title' => '',
    'after_title' => "\n",
);
register_sidebar($args404);

/*
 *=============================
 *       OBRAZKI
 *=============================
 */

add_theme_support('post-thumbnails');
set_post_thumbnail_size(1140, 500, true); //set proper thumbnail size


update_option('thumbnail_size_w', 375);
update_option('thumbnail_size_h', 228);
update_option('thumbnail_crop', 1);
update_option('medium_size_w', 570);
update_option('medium_size_h', 3000);
update_option('medium_crop', 0);
update_option('large_size_w', 1140);
update_option('large_size_h', 3000);
update_option('large_crop', 0);

add_image_size('post-thumbnails', 1920, 400, true);
add_image_size('slider', 1920, 800, true);
add_image_size('zespol-thumbnail', 150, 150, true);

function fpweb_get_image_id($image_url)
{
    global $wpdb;
    $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url));
    return $attachment[0];
}

function wp_get_attachment_image_or_svg($fileid, $size, $icon="", $imgatr){
    $fileulr = wp_get_attachment_url($fileid);
    $fileext = pathinfo($fileulr, PATHINFO_EXTENSION);

    if($fileext == 'svg'){
        $svg_file = file_get_contents($fileulr);
        $find_string   = '<svg';
        $position = strpos($svg_file, $find_string);
        $svg_file_xml = substr($svg_file, $position);
        $svg_file_xml = '<span class="'.$imgatr['class'].'">'.$svg_file_xml.'</span>';
        return $svg_file_xml;
    }else{
        return wp_get_attachment_image( $fileid, $size, $icon, $imgatr );
    }

}

/*
 *=============================
 * OBSŁUGA TŁUMACZEŃ
 *=============================
 */

add_action('after_setup_theme', 'my_theme_setup');
function my_theme_setup()
{
    load_theme_textdomain('theme', get_template_directory() . '/languages');
    load_theme_textdomain('engine', get_template_directory() . '/languages/framework/');
}

/*=============================
 * OPCJE SZABLONU
 *=============================
 */

function theme_customize_register( $wp_customize ) {
    //All our sections, settings, and controls will be added here
    //Adding a section
    $wp_customize->add_section(
        TEMPL_NAME,
        array(
            'title' => TEMPL_AUTHOR,
            'description' => 'Set custom data for this theme',
            'priority' => 9999,
        )
    );

    //Add a setting
    $wp_customize->add_setting(
        'theme_logo'
    );


    //Add control
    $wp_customize->add_control(
        new WP_Customize_Image_Control( $wp_customize, 'theme_logo', array(
            'label'    => __( 'Logo', 'engine' ),
            'section'  => TEMPL_NAME,
            'settings' => 'theme_logo',
        ) )
    );
    //Add a setting
    $wp_customize->add_setting(
        'theme_logo_svg'
    );
    //Add control
    $wp_customize->add_control(
        new WP_Customize_Image_Control( $wp_customize, 'theme_logo_svg', array(
            'label'    => __( 'Logo SVG', 'engine' ),
            'section'  => TEMPL_NAME,
            'settings' => 'theme_logo_svg',
        ) )
    );

    //Add a setting
    $wp_customize->add_setting(
        'footer_text'
    );

    //Add control
    $wp_customize->add_control(
        'footer_text',
        array(
            'label' => __('Footer text','engine'),
            'section' => TEMPL_NAME,
            'type' => 'text',
        )
    );

}
add_action( 'customize_register', 'theme_customize_register' );

/*=============================
 * WSTĘPY W POSTACH
 *=============================
 */

function custom_excerpt_length($length = 1000)
{
    return $length;
}

add_filter('excerpt_length', 'custom_excerpt_length', 999);

function new_excerpt_more($more)
{
    return null;
}
add_filter('excerpt_more', 'new_excerpt_more');

add_action('init', 'my_add_excerpts_to_pages');
function my_add_excerpts_to_pages()
{
    add_post_type_support('page', 'excerpt');
    add_post_type_support('post', 'excerpt');
}

function the_excerpt_max_charlength($charlength)
{
    $str ="";
    $excerpt = get_the_content();
    $excerpt = strip_tags($excerpt, '<br>');
    //$excerpt = str_replace('&nbsp;','', $excerpt);
    $excerpt = ltrim($excerpt, '&nbsp; ');
    $charlength++;

    if (mb_strlen($excerpt) > $charlength) {
        //$str = '<p>';
        $subex = mb_substr($excerpt, 0, $charlength - 5);
        $exwords = explode(' ', $subex);
        $excut = -(mb_strlen($exwords[count($exwords) - 1]));
        if ($excut < 0) {
            $str .= mb_substr($subex, 0, $excut);
        } else {
            $str .= $subex;
        }
        $str = rtrim($str, " .");
        $str .= "&nbsp;";
        $str .= '...';
    } else {
        $str .= $excerpt;
    }
    return $str;
}

/* simple title for all */
function get_simple_title()
{
    if (is_category()) {
        $title = sprintf('%s', single_cat_title('', false));
    } elseif (is_tag()) {
        $title = sprintf('%s', single_tag_title('', false));
    } elseif (is_author()) {
        $title = sprintf('%s', '<span class="vcard">' . get_the_author() . '</span>');
    } elseif (is_year()) {
        $title = sprintf('%s', get_the_date(_x('Y', 'yearly archives date format','theme')));
    } elseif (is_month()) {
        $title = sprintf('%s', get_the_date(_x('F Y', 'monthly archives date format', 'theme')));
    } elseif (is_day()) {
        $title = sprintf('%s', get_the_date(_x('F j, Y', 'daily archives date format', 'theme')));
    } elseif (is_tax('post_format')) {
        if (is_tax('post_format', 'post-format-aside', 'theme')) {
            $title = _x('Asides', 'post format archive title', 'theme');
        } elseif (is_tax('post_format', 'post-format-gallery')) {
            $title = _x('Galleries', 'post format archive title', 'theme');
        } elseif (is_tax('post_format', 'post-format-image')) {
            $title = _x('Images', 'post format archive title', 'theme');
        } elseif (is_tax('post_format', 'post-format-video')) {
            $title = _x('Videos', 'post format archive title', 'theme');
        } elseif (is_tax('post_format', 'post-format-quote')) {
            $title = _x('Quotes', 'post format archive title', 'theme');
        } elseif (is_tax('post_format', 'post-format-link')) {
            $title = _x('Links', 'post format archive title', 'theme');
        } elseif (is_tax('post_format', 'post-format-status')) {
            $title = _x('Statuses', 'post format archive title', 'theme');
        } elseif (is_tax('post_format', 'post-format-audio')) {
            $title = _x('Audio', 'post format archive title', 'theme');
        } elseif (is_tax('post_format', 'post-format-chat')) {
            $title = _x('Chats', 'post format archive title', 'theme');
        }
    } elseif (is_post_type_archive()) {
        $title = sprintf('%s', post_type_archive_title('', false));
    } elseif (is_tax()) {
        $tax = get_taxonomy(get_queried_object()->taxonomy);
        /* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
        $title = sprintf('%1$s: %2$s', $tax->labels->singular_name, single_term_title('', false));
    } else {
        $title = "";
    }

    /**
     * Filter the archive title.
     *
     * @since 4.1.0
     *
     * @param string $title Archive title to be displayed.
     */
    return apply_filters('get_the_archive_title', $title);
}

/*
/*=============================
 * WPML
 *=============================
 */
if (class_exists('SitePress')) {
    function language_selector_flags()
    {
        $languages = icl_get_languages('skip_missing=0&orderby=code');
        if (!empty($languages)) {
            foreach ($languages as $l) {
                if (!$l['active']) {
                    echo '<a href="' . $l['url'] . '">';
                    echo '<img src="' . $l['country_flag_url'] . '" height="12" alt="' . $l['language_code'] . '" width="18" />';
                    echo '</a>';
                }
            }
        }
    }

    function wpml_current_lang()
    {

        $languages = icl_get_languages('skip_missing=1');
        $curr_lang = array();
        if (!empty($languages)) {

            foreach ($languages as $language) {
                if (!empty($language['active'])) {
                    $curr_lang = $language; // This will contain current language info.
                    break;
                }
            }
        }

        return $curr_lang;
    }

    function languages_list_menu($currentlang = null)
    {
        $languages = icl_get_languages('skip_missing=0&orderby=KEY&order=DIR&link_empty_to=str');

        $ret = '<div class="lang-chose">';
        /*if($currentlang){
          //$ret .=  '<img src="'.$currentlang['country_flag_url'].'" alt="'.$currentlang['native_name'].'"> <i class="fa fa-caret-down" aria-hidden="true"></i>';
          $ret .=  '<img src="'.$currentlang['country_flag_url'].'" alt="'.$currentlang['native_name'].'">';
        }*/
        $ret .= '<ul class="lang-switcher">';
        foreach ($languages as $language) {
            $flag = $language['country_flag_url'];
            $url = $language['url'];
            $isActive = $language['active'];
            $name = $language['native_name'];
            if ($isActive == 1) {
                $class = 'class="active"';
            } else {
                $class = "";
            }

            $ret .= '
            <li>
              <a href="' . $url . '" ' . $class . '>
                <img src="' . $flag . '" alt="' . $name . '" />
              </a>
            </li>
            ';

        }
        $ret .= '</ul>';
        $ret .= '</div>';
        return $ret;
    }
} //end check class

function limit_words($string, $word_limit) {
    $words = explode(' ', $string);
    return implode(' ', array_slice($words, 0, $word_limit));
}


/********* TinyMCE Buttons ***********/



add_action( 'after_setup_theme', 'mytheme_theme_setup' );
if ( ! function_exists( 'mytheme_theme_setup' ) ) {
    function mytheme_theme_setup() {
        add_action( 'init', 'mytheme_buttons' );
    }
}


if ( ! function_exists( 'mytheme_buttons' ) ) {
    function mytheme_buttons() {
        // if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {
        //     return;
        // }
        //
        // if ( get_user_option( 'rich_editing' ) !== 'true' ) {
        //     return;
        // }
        add_filter( 'mce_external_plugins', 'mytheme_add_buttons' );
        add_filter( 'mce_buttons', 'mytheme_register_buttons' );
    }
}

if ( ! function_exists( 'mytheme_add_buttons' ) ) {
    function mytheme_add_buttons( $plugin_array ) {
        $plugin_array['mybutton'] = get_theme_file_uri().'/js/tinymce_buttons.js?'.date('U');
        return $plugin_array;
    }
}

if ( ! function_exists( 'mytheme_register_buttons' ) ) {
    function mytheme_register_buttons( $buttons ) {
        array_push( $buttons, 'mybutton' );
        return $buttons;
    }
}

if ( !function_exists( 'mytheme_tinymce_extra_vars' ) ) {
	function mytheme_tinymce_extra_vars() {
        ?>
		<script type="text/javascript">
			var tinyMCE_object = <?php echo json_encode(
				array(
                	'path_local' => get_theme_file_uri(),
					'button_name' => esc_html__('Shortcodes', 'axichem'),
					'button_title' => esc_html__('Add shortcode', 'axichem'),
					'image_title' => esc_html__('Image', 'axichem'),
					'image_button_title' => esc_html__('Upload', 'axichem'),
				)
				);
			?>;
		</script>
        <?php
	}
}
add_action ( 'after_wp_tiny_mce', 'mytheme_tinymce_extra_vars' );

//
// Crone

function isa_add_cron_recurrence_interval( $schedules ) {
  $schedules['every_fifteen_minutes'] = array(
	  'interval'  => 900,
	  'display'   => __('Every 15 Minutes', 'axichem')
  );
  return $schedules;
}
add_filter('cron_schedules', 'isa_add_cron_recurrence_interval');


if (!wp_next_scheduled('axichem_crone_hook')) {
    wp_schedule_event( time(), 'every_fifteen_minutes', 'axichem_crone_hook' );
}
add_action('axichem_crone_hook', 'axichem_crone' );

function axichem_crone() {
    require(getcwd() . '/wp-content/themes/axichem/crone/crone_share.php');
    require(getcwd() . '/wp-content/themes/axichem/crone/crone_feed.php');
}






function mce_formats( $init ) {
	$formats = array(
		'p'          => __( 'Paragraph', 'text-domain' ),
		'h1'         => __( 'Heading 1', 'text-domain' ),
		'h2'         => __( 'Heading 2', 'text-domain' ),
        'h3'         => __( 'Heading 3', 'text-domain' ),
        'h4'         => __( 'Heading 4', 'text-domain' ),
        'h5'         => __( 'Heading 5', 'text-domain' ),
        'h6'         => __( 'Heading 6', 'text-domain' ),
		// ... h3 - h6 ...
		'pre'        => __( 'Preformatted', 'text-domain' ),
		'paragraph-1' => __( '18px, Gray', 'text-domain' ),
		'paragraph-2' => __( '15px, Black', 'text-domain' ),
		'paragraph-4' => __( '22px, Bold, Black', 'text-domain' ),
		'paragraph-5' => __( '24px, Bold', 'text-domain' ),
		'paragraph-6' => __( '38px, Bold', 'text-domain' ),
		'paragraph-3' => __( 'GT Pressura Mono, Bold', 'text-domain' ),
	);

    // concat array elements to string
	array_walk( $formats, function ( $key, $val ) use ( &$block_formats ) {
		$block_formats .= esc_attr( $key ) . '=' . esc_attr( $val ) . ';';
	}, $block_formats = '' );

	$init['block_formats'] = $block_formats;

	return $init;
}
add_filter( 'tiny_mce_before_init', __NAMESPACE__ . '\\mce_formats' );
?>

<?php
add_filter( 'pre_get_posts', 'tgm_io_cpt_search' );
function tgm_io_cpt_search( $query ) {

    if ( $query->is_search ) {
	$query->set( 'post_type', array( 'post', 'news', 'page' ) );
    }

    return $query;

}


function remove_menus(){
  remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_menu', 'remove_menus' );
